﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ActivateToken.aspx.cs" Inherits="E_vent.ActivateToken" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>E-Vent</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>
        html {
            background-image: url("Contents/Images/fondGGJ.jpg");
            width: 100%;
            height: 100%;
        }

        .imgStatut {
            max-width: 269px;
            width: 70%;
            min-width: 50px;
        }
    </style>
</head>
<body>

    <div class="" style="width: 100%; position: fixed; top: 24%; height: auto !important;" id="emailPanel">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4"></div>
        <form class="col-xs-12 col-sm-6 col-md-6 col-lg-4" style="text-align: center; background-color: #313A4D; opacity: 0.7; padding: 10px 30px; padding-bottom: 10px;" id="form1" runat="server">
            <div class="row" style="">
                <div class="" style="">
                    <asp:Image CssClass="imgStatut" ID="ImageSuccess" Visible="false" runat="server" ImageUrl="~/Contents/Images/checked.png" />
                    <asp:Image CssClass="imgStatut" ID="ImageDenied" Visible="false" runat="server" ImageUrl="~/Contents/Images/wrong-access.png" />
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>

                    <div id="AuthenticationPanel" runat="server" style="color:white;">
                        <div class="form-group" style="text-align: center;">
                            <h3>Réinitialisation du mot de passe</h3>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword" class="sr-only">Nouveau mot de passe :</label>
                            <input runat="server" type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required="" autofocus="" />
                        </div>
                        <div class="form-group">
                            <label for="inputPassword2" class="sr-only">Confimer le mot de passe :</label>
                            <input runat="server" type="password" id="inputPassword2" class="form-control" placeholder="Confirmer le mot de passe" required="" />
                        </div>
                        <div class="form-group">
                            <asp:Button ID="ChangePassword" runat="server" CssClass="btn btn-success" Text="Changer le mot de passe" OnClick="ChangePassword_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</body>
</html>
