﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Log.CreateLogFile();      
        }

        protected void decoButton_Click(object sender, EventArgs e)
        {
            PageBase.Logout();
        }

        protected void TitleHeader_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Response.Redirect("~/Home.aspx");
        }
    }
}