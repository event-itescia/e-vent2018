﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_vent
{
    public class Evenement
    {
        public int id { get; set; }
        public string nom { get; set; }
        public DateTime dateDebut { get; set; }
        public DateTime dateFin { get; set; }
        public int codePostal { get; set; }
        public string ville { get; set; }
        public int annee { get; set; }
        public string pays { get; set; }
        public string rue { get; set; }
        public int numRue { get; set; }

        public Evenement()
        {
        }

        public Evenement(string nom)
        {
            this.nom = nom;
        }

        public Evenement(string nom, int codePostal, string ville, int annee, string pays, string rue, int numRue)
        {
            this.nom = nom;
            this.codePostal = codePostal;
            this.ville = ville;
            this.annee = annee;
            this.pays = pays;
            this.rue = rue;
            this.numRue = numRue;
        }

        public Evenement(string nom, int codePostal, string ville, string pays, string rue, DateTime dateDebut, DateTime dateFin)
        {
            this.nom = nom;
            this.codePostal = codePostal;
            this.ville = ville;
            this.annee = dateDebut.Year;
            this.pays = pays;
            this.rue = rue;
            this.dateDebut = dateDebut;
            this.dateFin = dateFin;
        }

        public Evenement(string nom, string ville, string pays, string rue, int numRue, DateTime dateDebut, DateTime dateFin)
        {
            this.nom = nom;
            this.ville = ville;
            this.annee = dateDebut.Year;
            this.pays = pays;
            this.rue = rue;
            this.numRue = numRue;
            this.dateDebut = dateDebut;
            this.dateFin = dateFin;
        }

        public Evenement(string nom, int codePostal, string ville, string pays, string rue, int numRue, DateTime dateDebut, DateTime dateFin)
        {
            this.nom = nom;
            this.codePostal = codePostal;
            this.ville = ville;
            this.annee = dateDebut.Year;
            this.pays = pays;
            this.rue = rue;
            this.numRue = numRue;
            this.dateDebut = dateDebut;
            this.dateFin = dateFin;
        }

        public Evenement(int id, string nom, int codePostal, string ville, string pays, string rue, int numRue, DateTime dateDebut, DateTime dateFin)
        {
            this.id = id;
            this.nom = nom;
            this.codePostal = codePostal;
            this.ville = ville;
            this.annee = dateDebut.Year;
            this.pays = pays;
            this.rue = rue;
            this.numRue = numRue;
            this.dateDebut = dateDebut;
            this.dateFin = dateFin;
        }


    }
    public static class EventManager
    {
        public static List<Evenement> getEvenementByAnnee(List<Evenement> listEvents, int annee)
        {
            List<Evenement> listEvenement = new List<Evenement>();
            try
            {
                if (listEvents != null && listEvents.Count() > 0 && annee > 1950 && annee <= 2200)
                {
                    listEvenement.AddRange(listEvents.Where(x => x.annee.Equals(annee)));
                }
            }
            catch (Exception ex)
            {

            }
            return listEvenement;
        }
    }
}