﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_vent
{
    public class Association
    {
        public int id { get; set; }
        public string nom { get; set; }
        public string mail { get; set; }
        public int tel { get; set; }
        public string siteWeb { get; set; }

        public Association()
        {

        }

        public Association(int id, string nom)
        {
            this.id = id;
            this.nom = nom;
        }

        public Association(int id, string nom, string mail, int tel, string siteWeb)
        {
            this.id = id;
            this.nom = nom;
            this.mail = mail;
            this.tel = tel;
            this.siteWeb = siteWeb;
        }
    }
}