﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class EditEvent : PageBase
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            isAuthenticated();
            if (!IsPostBack)
            {
                List<Evenement> allEvent = new List<Evenement>();
                rptEditEvent.DataSource = db.GetAllEvent();
                rptEditEvent.DataBind();
            }
        }

        protected void btnSearchEvent_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(inputSearchEvent.Value))
            {
                List<Evenement> listEvent = db.SearchEvent(inputSearchEvent.Value.Trim());
                if (listEvent.Count > 0)
                {
                    string message = string.Format("{0} résultat(s) trouvé avec le mot clé \"{1}\" !", listEvent.Count, inputSearchEvent.Value.Trim());
                    ShowMessage(message, "success");
                    rptEditEvent.DataSource = listEvent;
                    rptEditEvent.DataBind();
                }
                else
                {
                    string message = string.Format("Aucun résultat trouvé avec le mot clé \"{0}\"", inputSearchEvent.Value.Trim());
                    ShowMessage(message, "error");
                    rptEditEvent.DataSource = db.GetAllEvent();
                    rptEditEvent.DataBind();
                }
            }
            else
            {
                ShowMessage("Veuillez entrer un mot clé avant de lancer une recherche.", "warning");
                rptEditEvent.DataSource = db.GetAllEvent();
                rptEditEvent.DataBind();
            }
        }

        [System.Web.Services.WebMethod]
        public static string GetAjaxEvent(string idEvent)
        {
            DBConnect db2 = new DBConnect();
            string dataEvent = db2.GetAjaxEvent(idEvent);
            return dataEvent;
        }

        protected void editNameEvent_Click(object sender, EventArgs e)
        {

        }


        [System.Web.Services.WebMethod]
        public static List<string> btnSaveEditEvent_Click(Dictionary<string,string> listInfo)
        {
            Dictionary<string, string> allInfo = listInfo;
            List<string> data = new List<string>();
            DBConnect db2 = new DBConnect();
            string message = "";
            try
            {
                bool edited = db2.EditEvent(listInfo, out message);
                data.Add(edited.ToString());
                data.Add(message);
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return data;
        }

        protected void deleteEvent_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "";
                if(!string.IsNullOrEmpty(idEvent.Value))
                {
                    if(db.DelEvent(idEvent.Value.Trim(), out message))
                    {
                        ShowMessage(message, "success");
                    }
                    else
                    {
                        ShowMessage(message, "error");
                    }
                    rptEditEvent.DataSource = db.GetAllEvent();
                    rptEditEvent.DataBind();
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}