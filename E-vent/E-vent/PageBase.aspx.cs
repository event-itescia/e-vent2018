﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace E_vent
{
    public partial class PageBase : Page
    {
        static bool mailSent = false;


        public static bool isAuthenticated()
        {
            try
            {
                if (HttpContext.Current == null || HttpContext.Current.Session["Authenticated"] == null || !(bool)HttpContext.Current.Session["Authenticated"])
                {
                    HttpContext.Current.Response.Redirect(@"~/Login.aspx");
                }

            }
            catch (Exception ex)
            {

            }
            return true;
        }

        public static bool isLogged()
        {
            try
            {
                if (HttpContext.Current != null && HttpContext.Current.Session["Authenticated"] != null && (bool)HttpContext.Current.Session["Authenticated"])
                {
                    return true;
                }

            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public static string GetSha512(string textInput)
        {
            try
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] bytes = Encoding.UTF8.GetBytes(textInput);
                byte[] hash = sha512.ComputeHash(bytes);

                StringBuilder result = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    result.Append(hash[i].ToString("X2"));
                }
                return result.ToString();
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        public static string GetToken()
        {
            return GetSha512(Convert.ToBase64String(Guid.NewGuid().ToByteArray()));
        }

        protected static void Login(string login, string password, bool isAdmin)
        {
            try
            {
                HttpContext.Current.Session.Add("Authenticated", true);
                HttpContext.Current.Session.Add("login", login);
                HttpContext.Current.Session.Add("password", password);
                HttpContext.Current.Session.Add("isAdmin", isAdmin);
                HttpContext.Current.Response.Redirect("~/Home.aspx");
            }
            catch (Exception ex)
            {

            }
        }

        public static bool isAdmin(string login512, string password512)
        {
            try
            {
                if (!string.IsNullOrEmpty(login512) && !string.IsNullOrEmpty(password512))
                {
                    if (!File.Exists(@"C:\APPDATA\auth.xml"))
                    {
                        new XDocument(new XElement("root", new XElement("login", GetSha512(Encrypt("admin"))), new XElement("password", GetSha512(Encrypt("root"))))).Save(@"C:\APPDATA\auth.xml");
                    }
                    XmlDocument doc = new XmlDocument();
                    doc.Load(@"C:\APPDATA\auth.xml");
                    string login = doc.DocumentElement.SelectSingleNode("/root/login").InnerText;
                    string pwd = doc.DocumentElement.SelectSingleNode("/root/password").InnerText;
                    if (login == login512 && pwd == password512)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            mailSent = true;
        }

        public static bool SendToken(string emailTo, out string error)
        {
            error = string.Empty;
            try
            {
                DBConnect db = new DBConnect();
                bool userExist = db.UserExist(emailTo);
                if(userExist)
                {
                    string tokenUser = db.GetTokenUser(emailTo);
                    if (!string.IsNullOrEmpty(tokenUser))
                    {
                        SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                        client.EnableSsl = true;
                        MailAddress adresseMailEnvoyeur = new MailAddress("EventStaff2018@gmail.com");
                        MailAddress Receveur = new MailAddress(emailTo);
                        MailMessage ConfigMess = new MailMessage(adresseMailEnvoyeur, Receveur);
                        ConfigMess.Subject = "Mot de passe perdu";
                        ConfigMess.IsBodyHtml = true; // si tu veux activer le html dans ton body
                        string url = string.Format("http://localhost:63092/ActivateToken.aspx?user={0}&token={1}", emailTo, tokenUser);
                        ConfigMess.Body = "<h4>Pour redéfinir votre mot de passe, cliquez <a href=" + url + ">ICI</a></h4>";
                        NetworkCredential user = new NetworkCredential("EventStaff2018@gmail.com", "2018EVENT2018");
                        client.Credentials = user;
                        //client.EnableSsl = true;
                        client.Send(ConfigMess);
                        return true;
                    }
                    else
                    {
                        error = string.Format("{0} ne possède aucun token. Merci de contacter un administrateur pour résoudre ce problème.", emailTo);
                    }
                }
                else
                {
                    error = string.Format("Aucun utilisateur correspondant à {0} dans la base de donnée.", emailTo);
                }
                
                

                
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public void ShowMessage(string Message, string typeErreur)
        {
            try
            {
                Console.Out.Write("debut");
                ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), @"ShowMessage('" + Message + "','" + typeErreur.ToLower() + "')", true);
                Console.Out.Write("fin");
            }
            catch (Exception ex)
            {

            }

        }

        public static string Encrypt(string input)
        {
            byte[] inputArray = UTF8Encoding.UTF8.GetBytes(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes("2018-3hn8-e-vent");
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string input)
        {
            byte[] inputArray = Convert.FromBase64String(input);
            TripleDESCryptoServiceProvider tripleDES = new TripleDESCryptoServiceProvider();
            tripleDES.Key = UTF8Encoding.UTF8.GetBytes("2018-3hn8-e-vent");
            tripleDES.Mode = CipherMode.ECB;
            tripleDES.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tripleDES.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(inputArray, 0, inputArray.Length);
            tripleDES.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public static void Logout()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Response.Redirect("~/Login.aspx");
            }
        }
    }
}