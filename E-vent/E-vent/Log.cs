﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace E_vent
{
    public static class Log
    {
        public static bool CreateLogFile()
        {
            try
            {
                if (!File.Exists(@"C:/APPDATA/logError.txt"))
                {
                    StreamWriter file = new StreamWriter(@"C:/APPDATA/logError.txt");
                    DateTime localDate = DateTime.Now;
                    var culture = new CultureInfo("fr-FR");
                    file.WriteLine(localDate.ToString(culture) + " : Creation Log File.");
                    file.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public static void Write(string log)
        {
            try
            {
                if (!File.Exists(@"C:/APPDATA/logError.txt"))
                {
                    StreamWriter file = new StreamWriter(@"C:/APPDATA/logError.txt");
                    DateTime localDate = DateTime.Now;
                    var culture = new CultureInfo("fr-FR");
                    file.WriteLine(localDate.ToString(culture) + " : Creation Log File.");
                    file.Close();
                }
                using (StreamWriter file = new StreamWriter(@"C:/APPDATA/logError.txt", append: true))
                {
                    file.WriteLine(DateTime.Now.ToString(new CultureInfo("fr-FR")) + " : " + log);
                }
            }
            catch (Exception ex)
            {

            }
        }


    }
}