﻿<%@ Page Title="Edition d'évènement" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EditEvent.aspx.cs" Inherits="E_vent.EditEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
                        </asp:ScriptManager>--%>
    <div class="">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Edition d'évènement</h3>
                <div class="" style="margin-top: 10px;">
                    <input type="text" placeholder="Search.." name="search" onfocus="this.value=''" id="inputSearchEvent" runat="server" />
                    <asp:Button ID="btnSearchEvent" runat="server" OnClick="btnSearchEvent_Click" Text="Rechercher"></asp:Button>
                </div>
            </div>
            <div class="panel-body">
                <asp:Repeater ID="rptEditEvent" runat="server">
                    <HeaderTemplate>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nom</th>
                                        <th>Adresse</th>
                                        <th style="text-align: center;">Début</th>
                                        <th style="text-align: center;">Fin</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                    </HeaderTemplate>

                    <ItemTemplate>
                        <tr>
                            <td><%# ((E_vent.Evenement)Container.DataItem).id %></td>
                            <td><%# ((E_vent.Evenement)Container.DataItem).nom %></td>
                            <td><%# ((E_vent.Evenement)Container.DataItem).numRue %> <%#((E_vent.Evenement)Container.DataItem).rue %>, <%#((E_vent.Evenement)Container.DataItem).ville %>, <%#((E_vent.Evenement)Container.DataItem).pays %></td>
                            <td style="text-align: center;"><%#((E_vent.Evenement)Container.DataItem).dateDebut %></td>
                            <td style="text-align: center;"><%#((E_vent.Evenement)Container.DataItem).dateFin %></td>
                            <td style="text-align: center;">
                                <button name="btnEdit" type="button" class="btn btn-warning" data-toggle="modal" data-id="<%# ((E_vent.Evenement)Container.DataItem).id %>" data-target="#myModal">Editer</button>
                                <button name="btnDelete" type="button" class="btn btn-danger" data-toggle="modal" data-id="<%# ((E_vent.Evenement)Container.DataItem).id %>" data-target="#confirmDeleteModal">Supprimer</button>
                            </td>
                        </tr>
                    </ItemTemplate>

                    <FooterTemplate>
                        </tbody>
                        </table>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>

                <asp:HiddenField ID="idEvent" runat="server" ClientIDMode="static" />

                <!-- Modal -->
                <div id="confirmDeleteModal" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h2 class="modal-title">Suppression</h2>
                            </div>
                            <div class="modal-body">
                                <h3 style="color: red;">Attention</h3>
                                <br />
                                <p>En appuyant sur le bouton "Supprimer" vous supprimerez définitivement cet évènement.</p>
                                <p>Tout les membres inscrit à cet évènement seront également supprimés.</p>
                            </div>
                            <div class="modal-footer">
                                <button id="cancelSuppression" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                <asp:Button ID="deleteEvent" runat="server" OnClick="deleteEvent_Click" CssClass="btn btn-danger" Text="Supprimer" />
                            </div>
                        </div>

                    </div>
                </div>


                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <%  %>
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" id="titleModal">Edition d'évènement</h4>

                            </div>
                            <div class="modal-body">

                                <%--NOM--%>
                                <div class="form-group">
                                    <label style="font-weight: 100;" for="eventName">Nom de l'évènement : </label>
                                    <input type="text" class="form-control" id="eventName" />
                                    <label id="oldeventName" class="hidden" />
                                </div>

                                <%--ADRESSE--%>
                                <div class="form-group">
                                    <label style="font-weight: 100;" for="eventAdresse">Adresse de l'évènement : </label>
                                    <div>
                                        <div class="form-group col-xs-6 col-sm-3 col-md-3 col-lg-3">
                                            <label>Numéro de rue</label>
                                            <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputNumberStreet" />
                                        </div>
                                        <div class="form-group col-xs-6 col-sm-9 col-md-9 col-lg-9">
                                            <label>Nom de rue</label>
                                            <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputNameStreet" />
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <label>Ville</label>
                                            <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputlocality" />
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <label>Code postal</label>
                                            <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputPostalCode" />
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <label>Pays</label>
                                            <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputCountry" />
                                        </div>
                                    </div>
                                </div>

                                <%--Date--%>
                                <div id="formDate" class="form-group">
                                    <label style="font-weight: 100;">Période : </label>
                                    <input id="dateRangePicker" class="form-control" type="text" name="datetimes" />
                                    <asp:HiddenField ClientIDMode="Static" ID="dateRangeEvent" runat="server" />
                                </div>

                                <asp:HiddenField ID="startEvent" Value="" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="endEvent" Value="" ClientIDMode="Static" runat="server" />

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                <button id="btnSaveChangeEvent" type="button" class="btn btn-success">Enregistrer</button>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
    <script>

        function EnterKey(e) {
            //if (e.keyCode == 13) {
            //    var div = e.currentTarget;
            //    var idDiv = div.id;
            //    var newContent = $("#" + idDiv).val();
            //    var oldContent = document.getElementById("old" + idDiv).value;
            //    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };

            //    if (newContent != "") {
            //        if (idDiv == "eventDebut" || idDiv == "eventFin") {
            //            $("#" + idDiv).replaceWith('<label class="editable" id="' + idDiv + '" onkeypress="return EnterKey(event)">' + new Date(newContent).toLocaleDateString('fr-FR', options) + '</label>');
            //            document.getElementById("old" + idDiv).value = newContent;
            //            $(".bootstrap-datetimepicker-widget.dropdown-menu.bottom").remove();
            //        }
            //        else {
            //            $("#" + idDiv).replaceWith('<label class="editable" id="' + idDiv + '" onkeypress="return EnterKey(event)">' + newContent + '</label>');
            //            document.getElementById("old" + idDiv).value = newContent;
            //        }

            //    }
            //    else {
            //        $("#" + idDiv).replaceWith('<label class="editable" id="' + idDiv + '" onkeypress="return EnterKey(event)">' + oldContent + '</label>');
            //    }
            //}
        }

        $(document).on("click", "button[name='btnDelete']", function () {
            var idEvent = $(this).data('id');
            $("#idEvent").val(idEvent);
        });

        $(document).on("click", "button[name='btnEdit']", function () {
            var idEvent = $(this).data('id');
            $("#idEvent").val(idEvent);

            PageMethods.GetAjaxEvent(idEvent, onSuccess, onError);

            function onSuccess(result) {
                var event = JSON.parse(result);
                console.log(event);
                var adresseComplete = event.numRue + " " + event.rue + ", " + event.ville + " (" + event.codePostal + ") ," + event.pays;
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
                var dateDebutParsed = parseInt(event.dateDebut.replace(/[^0-9]/g, ""));
                var dateFinParsed = parseInt(event.dateFin.replace(/[^0-9]/g, ""));
                var dateDebut = new Date(dateDebutParsed);
                var dateFin = new Date(dateFinParsed);
                var startEvent = dateDebut.toLocaleDateString("fr") + " " + dateDebut.getHours() + ":" + dateDebut.getMinutes();
                var endEvent = dateFin.toLocaleDateString("fr") + " " + dateFin.getHours() + ":" + dateFin.getMinutes();

                document.getElementById("eventName").value = event.nom;
                $("#street_number").val(event.numRue);
                $("#route").val(event.rue);
                $("#locality").val(event.ville);
                $("#administrative_area_level_1").val();
                $("#country").val(event.pays);
                $("#postal_code").val(event.codePostal);
                $("#startEvent").val(startEvent);
                $("#endEvent").val(endEvent);

                if (event.numRue != 0) {
                    $("#inputNumberStreet").val(event.numRue);
                    $("#inputNumberStreet").text(event.numRue);
                }
                else {
                    $("#inputNumberStreet").val("");
                    $("#inputNumberStreet").text("");
                }

                $("#inputNameStreet").val(event.rue);
                $("#inputlocality").val(event.ville);
                $("#inputPostalCode").val(event.codePostal);
                $("#inputCountry").val(event.pays);

                $("#eventAdresse").val(adresseComplete);

                $('#dateRangePicker').daterangepicker({
                    timePicker24Hour: true,
                    timePicker: true,
                    "minYear": 2018,
                    "maxYear": 2100,
                    "startDate": startEvent,
                    "endDate": endEvent,
                    "locale": {
                        "format": "DD/MM/YYYY HH:mm",
                        "separator": " --> ",
                        "applyLabel": "Valider",
                        "cancelLabel": "Annuler",
                        "fromLabel": "De",
                        "toLabel": "à",
                        "customRangeLabel": "Custom",
                        "weekLabel": "W",
                        "daysOfWeek": [
                            "Lun",
                            "Mar",
                            "Mer",
                            "Jeu",
                            "Ven",
                            "Sam",
                            "Dim"
                        ],
                        "monthNames": [
                            "Janvier",
                            "Février",
                            "Mars",
                            "Avril",
                            "Mai",
                            "Juin",
                            "Juillet",
                            "Août",
                            "Septembre",
                            "Octobre",
                            "Novembre",
                            "Decembre"
                        ],
                        "firstDay": 1
                    },
                    "parentEl": "formDate",
                    "cancelClass": "btn-danger"

                }, function (start, end, label) {
                    document.getElementById("startEvent").value = start.format('DD/MM/YYYY HH:mm');
                    document.getElementById("endEvent").value = end.format('DD/MM/YYYY HH:mm');
                    return;
                });
            }

            function onError(result) {
                alert('Cannot process your request at the moment, please try later.');
            }
        });

        $(document).on("click", "#btnSaveChangeEvent", function () {

            if (document.getElementById("inputNumberStreet").value == "")
            {
                document.getElementById("inputNumberStreet").value = "0";
            }

            var listInfoEvent = {
                id: document.getElementById("idEvent").value,
                name: document.getElementById("eventName").value,
                numberStreet: document.getElementById("inputNumberStreet").value,
                nameStreet: document.getElementById("inputNameStreet").value,
                city: document.getElementById("inputlocality").value,
                postalCode: document.getElementById("inputPostalCode").value,
                country: document.getElementById("inputCountry").value,
                startEvent: document.getElementById("startEvent").value,
                endEvent: document.getElementById("endEvent").value
            };

            var empty = false;
            for (var i in listInfoEvent) {
                if (listInfoEvent[i] == null || listInfoEvent[i] == "") {
                    empty = true;
                }
            }

            if (!empty) {
                PageMethods.btnSaveEditEvent_Click(listInfoEvent, onSuccessEdit, onErrorEdit);
            }



            function onSuccessEdit(result) {
                console.log(result);
                if (result[0] == "True") {
                    $('#myModal').modal('toggle');
                    ShowMessage(result[1], "success");
                }
                else {
                    ShowMessage(result[1], "error");
                }


            }

            function onErrorEdit(result) {
                alert('Cannot process your request at the moment, please try later.');
            }
        });

        function ShowMessage(message, messagetype) {
            console.log(message);
            console.log(messagetype);
            var cssclass;
            switch (messagetype) {
                case 'success':
                    cssclass = 'alert-success';
                    break;
                case 'error':
                    cssclass = 'alert-danger';
                    break;
                case 'warning':
                    cssclass = 'alert-warning';
                    break;
                default:
                    cssclass = 'alert-info';
            }
            $('#Body').before('<div id="alert_div" style="position: fixed; z-index: 999; left: 50%;width: 50%;margin-top: 20px;transform: translateX(-50%);" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>');
            $(".alert").fadeTo(5000, 0).slideUp(1500, function () {
                $(this).remove();
            });
        }

    </script>
</asp:Content>
