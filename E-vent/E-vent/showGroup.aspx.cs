﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class showGroup : PageBase
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            isAuthenticated();
            if (!IsPostBack)
            {
                List<Groupe> allGroups = new List<Groupe>();
                rptShowGroup.DataSource = db.GetAllGroup();
                rptShowGroup.DataBind();
            }
        }
    }
}