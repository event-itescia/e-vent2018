using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
//Add MySql Library
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Security.Cryptography;
using System.Linq;
using System.Web.UI;
using System.Web.Script.Serialization;

namespace E_vent
{
    public class DBConnect
    {
        public MySqlConnection connection { get; set; }
        private string server;
        private string database;
        private string uid;
        private string password;

        #region Setup
        //Constructor
        public DBConnect()
        {
            Initialize();
        }

        //Initialize values
        public void Initialize()
        {
            server = "localhost";
            database = "eventdb";
            uid = "root";
            password = "";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                        //.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        //MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                Log.Write(ex.Message);
                return false;
            }
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                //MessageBox.Show(ex.Message);
                return false;
            }
        }
        #endregion


        public bool CheckToken(string emailUser, string token)
        {
            try
            {
                if (!string.IsNullOrEmpty(emailUser) && !string.IsNullOrEmpty(token))
                {
                    string queryCheckToken = string.Format("select count(*) from user where email = '{0}' and token = '{1}'", emailUser, token);
                    if (OpenConnection())
                    {
                        MySqlCommand cmdCheckToken = new MySqlCommand(queryCheckToken, connection);
                        int Count = int.Parse(cmdCheckToken.ExecuteScalar() + "");
                        this.CloseConnection();
                        if (Count == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool IsAdmin(string email, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
                {
                    string query = "SELECT Count(*) from user where login = '" + email.Trim() + "' and password = '" + password.Trim() + "'";
                    if (OpenConnection() == true)
                    {
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        int Count = int.Parse(cmd.ExecuteScalar() + "");
                        this.CloseConnection();
                        if (Count == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool UserExist(string email)
        {
            try
            {
                if (!string.IsNullOrEmpty(email))
                {
                    string query = "SELECT Count(*) from user where email = '" + email.Trim() + "'";
                    if (OpenConnection() == true)
                    {
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        int Count = int.Parse(cmd.ExecuteScalar() + "");
                        this.CloseConnection();
                        if (Count == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool AddUser(string login512, string password512, string email)
        {
            try
            {
                if (!string.IsNullOrEmpty(login512) && !string.IsNullOrEmpty(password512) && !string.IsNullOrEmpty(email))
                {
                    string queryCheckUser = string.Format("select count(*) from user where email = '{0}'", email);
                    if (OpenConnection())
                    {
                        MySqlCommand cmdCheckUser = new MySqlCommand(queryCheckUser, connection);
                        int Count = int.Parse(cmdCheckUser.ExecuteScalar() + "");
                        if (Count == 0)
                        {
                            string token = PageBase.GetToken();
                            string queryAddUser = string.Format("insert into user (login, password, email, token) values ('{0}', '{1}', '{2}', '{3}')"
                                , login512, password512, email, token);

                            MySqlCommand cmdAddUser = new MySqlCommand(queryAddUser, connection);
                            cmdAddUser.ExecuteNonQuery();
                            this.CloseConnection();
                            return true;
                        }
                        this.CloseConnection();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool AddUser(string login512, string password512, string email, out string message)
        {
            message = "";
            try
            {
                if (!string.IsNullOrEmpty(login512) && !string.IsNullOrEmpty(password512) && !string.IsNullOrEmpty(email))
                {
                    string queryCheckUser = string.Format("select count(*) from user where email = '{0}'", email);
                    if (OpenConnection())
                    {
                        MySqlCommand cmdCheckUser = new MySqlCommand(queryCheckUser, connection);
                        int Count = int.Parse(cmdCheckUser.ExecuteScalar() + "");
                        if (Count == 0)
                        {
                            string token = PageBase.GetToken();
                            string queryAddUser = string.Format("insert into user (login, password, email, token) values ('{0}', '{1}', '{2}', '{3}')"
                                , login512, password512, email, token);

                            MySqlCommand cmdAddUser = new MySqlCommand(queryAddUser, connection);
                            cmdAddUser.ExecuteNonQuery();
                            this.CloseConnection();
                            message = "User added";
                            return true;
                        }
                        message = "L'utilisateur existe d�j� dans la base de donn�e.";
                        this.CloseConnection();
                        return false;
                    }
                    message = "Impossible d'acceder � OpenConnection()";
                    return false;
                }
                message = "Un des champs en entr�e est null ou vide.";
            }
            catch (Exception ex)
            {
                message = "Exception intercepted : \"" + ex.Message + "\"";
            }
            return false;
        }

        public bool DelUser(string login512, string email)
        {
            try
            {
                if (!string.IsNullOrEmpty(login512) && !string.IsNullOrEmpty(email))
                {
                    string queryDelUser = string.Format("delete from user where login = {0} and email = {1}", login512, email);
                    if (OpenConnection())
                    {
                        MySqlCommand cmdDelUser = new MySqlCommand(queryDelUser, connection);
                        cmdDelUser.ExecuteNonQuery();
                        this.CloseConnection();
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool DelUser(string login512, string email, out string message)
        {
            message = "";
            try
            {
                if (!string.IsNullOrEmpty(login512) && !string.IsNullOrEmpty(email))
                {
                    string queryDelUser = string.Format("delete from user where login = '{0}' and email = '{1}'", login512, email);
                    if (OpenConnection())
                    {
                        MySqlCommand cmdDelUser = new MySqlCommand(queryDelUser, connection);
                        cmdDelUser.ExecuteNonQuery();
                        this.CloseConnection();
                        message = "User deleted";
                        return true;
                    }
                    message = "Impossible d'acceder � OpenConnection()";
                    return false;
                }
                message = "Un des champs en entr�e est null ou vide.";
                return false;
            }
            catch (Exception ex)
            {
                message = "Exception intercepted : \"" + ex.Message + "\"";
            }
            return false;
        }

        public bool DelEvent(string idEvent, out string message)
        {
            message = string.Empty;
            try
            {
                if(!string.IsNullOrEmpty(idEvent))
                {
                    string queryUnsubscribeMember = string.Format("DELETE FROM personne where idEvenement = '{0}'", idEvent);
                    string queryDelEvent = string.Format("DELETE FROM evenement WHERE id='{0}'", idEvent);

                    if (OpenConnection())
                    {
                        MySqlCommand cmdUnsubscribeMember = new MySqlCommand(queryUnsubscribeMember, connection);
                        cmdUnsubscribeMember.ExecuteNonQuery();

                        MySqlCommand cmd = new MySqlCommand(queryDelEvent, connection);
                        cmd.ExecuteNonQuery();

                        this.CloseConnection();
                        message = "Evenement supprim� avec succ�s.";
                        return true;
                    }
                    message = "Impossible d'acceder � la base de donn�e";
                    return false;
                }
                message = "L'�v�nement est inconnu. Essayez de recharger la page.";
                return false;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Write(ex.Message);
            }
            return false;
        }

        public bool ChangeLoginAdmin(string oldLogin, string newLogin, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(oldLogin) && !string.IsNullOrEmpty(newLogin) && !string.IsNullOrEmpty(password))
                {
                    string queryCount = "SELECT Count(*) from user where login = '" + oldLogin.Trim() + "' and password = '" + password.Trim() + "'";
                    string queryChange = "UPDATE user SET login='" + newLogin.Trim() + "' WHERE login = '" + oldLogin.Trim() + "'";
                    if (OpenConnection())
                    {
                        MySqlCommand cmdCount = new MySqlCommand(queryCount, connection);
                        int Count = int.Parse(cmdCount.ExecuteScalar() + "");
                        if (Count == 1)
                        {
                            MySqlCommand cmd = new MySqlCommand();

                            cmd.CommandText = queryChange;

                            cmd.Connection = connection;

                            cmd.ExecuteNonQuery();

                            this.CloseConnection();

                            return true;
                        }
                        this.CloseConnection();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool ReinitializePassword(string email, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
                {
                    string query = string.Format("SELECT Count(*) from user where email = '{0}'", email.Trim());
                    MySqlCommand cmdCheckUser = new MySqlCommand(query, connection);
                    if (this.OpenConnection())
                    {
                        int Count = int.Parse(cmdCheckUser.ExecuteScalar() + "");
                        if (Count == 1)
                        {
                            string queryChangeUserPassword = string.Format("UPDATE user SET password='{0}' WHERE email = '{1}'", password, email);
                            MySqlCommand cmd = new MySqlCommand(queryChangeUserPassword, connection);
                            cmd.ExecuteNonQuery();
                            string queryChangeUserToken = string.Format("UPDATE user SET token='{0}' WHERE email = '{1}'", PageBase.GetToken(), email);
                            MySqlCommand cmdChangeToken = new MySqlCommand(queryChangeUserToken, connection);
                            cmdChangeToken.ExecuteNonQuery();
                            this.CloseConnection();
                            return true;
                        }
                    }
                    this.CloseConnection();
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public bool ReinitializePassword(string email, string password, out string message)
        {
            try
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
                {
                    string query = string.Format("SELECT Count(*) from user where email = '{0}'", email.Trim());
                    MySqlCommand cmdCheckUser = new MySqlCommand(query, connection);
                    if (this.OpenConnection())
                    {
                        int Count = int.Parse(cmdCheckUser.ExecuteScalar() + "");
                        if (Count == 1)
                        {
                            string queryChangeUserPassword = string.Format("UPDATE user SET password='{0}' WHERE email = '{1}'", password, email);
                            MySqlCommand cmd = new MySqlCommand(queryChangeUserPassword, connection);
                            cmd.ExecuteNonQuery();
                            string queryChangeUserToken = string.Format("UPDATE user SET token='{0}' WHERE email = '{1}'", PageBase.GetToken(), email);
                            MySqlCommand cmdChangeToken = new MySqlCommand(queryChangeUserToken, connection);
                            cmdChangeToken.ExecuteNonQuery();
                            this.CloseConnection();
                            message = "Mot de passe r�initialis�.";
                            return true;
                        }
                        message = "L'utilisateur n'existe pas.";
                        CloseConnection();
                        return false;
                    }
                    this.CloseConnection();
                    message = "Impossible d'acceder � OpenConnection()";
                    return false;
                }
                message = "Un des champs en entr�e est null ou vide.";
                return false;
            }
            catch (Exception ex)
            {
                message = "Exception intercepted : \"" + ex.Message + "\"";
            }
            return false;
        }

        public bool ChangePasswordAdmin(string login, string oldPassword, string newPassword)
        {
            try
            {
                if (!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(oldPassword) && !string.IsNullOrEmpty(newPassword))
                {
                    string queryCount = "SELECT Count(*) from user where login = 'admin' and password = '" + oldPassword.Trim() + "'";
                    string queryChange = "UPDATE user SET password='" + newPassword.Trim() + "' WHERE login = '" + login + "'";

                    if (this.OpenConnection() == true)
                    {
                        MySqlCommand cmdCount = new MySqlCommand(queryCount, connection);
                        int Count = int.Parse(cmdCount.ExecuteScalar() + "");
                        if (Count == 1)
                        {
                            MySqlCommand cmd = new MySqlCommand();

                            cmd.CommandText = queryChange;

                            cmd.Connection = connection;

                            cmd.ExecuteNonQuery();

                            this.CloseConnection();

                            return true;
                        }
                        this.CloseConnection();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public string GetTokenUser(string emailUser)
        {
            string token = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(emailUser))
                {
                    string queryGetUserToken = string.Format("select token from user where email = '{0}'", emailUser);
                    if (OpenConnection())
                    {
                        MySqlCommand cmdGetUserToken = new MySqlCommand(queryGetUserToken, connection);

                        MySqlDataReader dataReader = cmdGetUserToken.ExecuteReader();
                        if (!dataReader.Read())
                            throw new InvalidOperationException("No records were returned.");

                        token = dataReader.GetString("token");

                        if (dataReader.Read())
                            throw new InvalidOperationException("Multiple records were returned.");
                        return token;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return token;
        }

        public List<Personne> GetAllPersonne()
        {
            List<Personne> listPersonne = new List<Personne>();
            string query = "select * from Personne order by prenom";
            try
            {
                if (OpenConnection() == true)
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        int id = int.Parse(dataReader["id"].ToString());
                        string nom = dataReader["nom"].ToString();
                        string prenom = dataReader["prenom"].ToString();
                        string email = dataReader["adrMail"].ToString();
                        string tel = dataReader["numTel"].ToString();
                        string sexe = dataReader["sexe"].ToString();
                        DateTime dateNaiss = Convert.ToDateTime(dataReader["dateNaissance"].ToString());
                        string poste = dataReader["poste"].ToString();
                        string adressePostale = dataReader["adrPost"].ToString();
                        int codePostal = int.Parse(dataReader["codePostal"].ToString());
                        string ville = dataReader["ville"].ToString();
                        string commentaire = dataReader["commentaire"].ToString();
                        bool isPayed = (bool)dataReader["isPayed"];
                        string taille = dataReader["taille"].ToString();
                        string regime = dataReader["regimeAlimentaire"].ToString();
                        bool premiereFois = (bool)dataReader["firstParticipation"];
                        string photo = dataReader["photo"].ToString();

                        Personne personne = new Personne(id, nom, email, tel, prenom, sexe, dateNaiss, poste, adressePostale, codePostal, ville, commentaire, isPayed, taille, regime, premiereFois, photo);
                        listPersonne.Add(personne);
                    }

                    //close Data Reader
                    dataReader.Close();

                    //close Connection
                    this.CloseConnection();

                    //return list to be displayed
                    return listPersonne;
                }
            }
            catch (Exception ex)
            {
                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return listPersonne;
            }
            return listPersonne;
        }

        public string GetPersonneById(string idPersonne)
        {
            CultureInfo Provider = CultureInfo.InvariantCulture;
            try
            {
                if (!string.IsNullOrEmpty(idPersonne))
                {
                    string queryCheckPers = string.Format("select count(*) from personne where id={0}", idPersonne);
                    if (OpenConnection())
                    {
                        MySqlCommand cmd = new MySqlCommand(queryCheckPers, connection);

                        //ExecuteScalar will return one value
                        int Count = int.Parse(cmd.ExecuteScalar() + "");
                        if (Count == 1)
                        {
                            string queryGetPers = string.Format("select * from personne where id={0}", idPersonne);
                            MySqlCommand cmdget = new MySqlCommand(queryGetPers, connection);
                            //Create a data reader and Execute the command
                            MySqlDataReader dataReader = cmdget.ExecuteReader();

                            //Read the data and store them in the list
                            while (dataReader.Read())
                            {
                                string nom = dataReader["nom"].ToString();
                                string prenom = dataReader["prenom"].ToString();
                                string sexe = dataReader["sexe"].ToString();
                                string dateNaisstring = dataReader["dateNaissance"].ToString().Replace('-', '/');
                                DateTime dateNaiss = DateTime.ParseExact(dateNaisstring, "dd/MM/yyyy HH:mm:ss", Provider);
                                string poste = dataReader["poste"].ToString();
                                string adrMail = dataReader["adrMail"].ToString();
                                string numTel = (dataReader["numTel"].ToString());
                                string adrPost = dataReader["adrPost"].ToString();
                                int codePostal = int.Parse(dataReader["codePostal"].ToString());
                                string ville = dataReader["ville"].ToString();
                                string commentaire = dataReader["commentaire"].ToString();
                                bool isPayed = (bool)dataReader["isPayed"];
                                string taille = dataReader["taille"].ToString();
                                string regimeAlimentaire = dataReader["regimeAlimentaire"].ToString();
                                bool firstParticipation = (bool)dataReader["firstParticipation"];
                                string photo = dataReader["photo"].ToString();

                                Personne laPersonne = new Personne(nom, adrMail, numTel, prenom, sexe, dateNaiss, poste, adrPost, codePostal, ville, commentaire, isPayed, taille, regimeAlimentaire, firstParticipation, photo);
                                string result = new JavaScriptSerializer().Serialize(laPersonne);
                                return result;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        public List<Personne> GetAllPersonneByEvent(string evenement)
        {
            List<Personne> listPersonne = new List<Personne>();
            try
            {
                if (!string.IsNullOrEmpty(evenement))
                {
                    string query = "select * from personne p join evenement e on p.idEvenement = e.id where e.nom = '" + evenement.Trim() + "' order by p.nom";
                    if (OpenConnection() == true)
                    {
                        //Create Command
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        //Create a data reader and Execute the command
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        //Read the data and store them in the list
                        while (dataReader.Read())
                        {
                            int id = int.Parse(dataReader["id"].ToString());
                            string nom = dataReader["nom"].ToString();
                            string prenom = dataReader["prenom"].ToString();
                            string email = dataReader["adrMail"].ToString();
                            string tel = dataReader["numTel"].ToString();
                            string sexe = dataReader["sexe"].ToString();
                            DateTime dateNaiss = Convert.ToDateTime(dataReader["dateNaissance"].ToString());
                            string poste = dataReader["poste"].ToString();
                            string adressePostale = dataReader["adrPost"].ToString();
                            int codePostal = int.Parse(dataReader["codePostal"].ToString());
                            string ville = dataReader["ville"].ToString();
                            string commentaire = dataReader["commentaire"].ToString();
                            bool isPayed = (bool)dataReader["isPayed"];
                            string taille = dataReader["taille"].ToString();
                            string regime = dataReader["regimeAlimentaire"].ToString();
                            bool premiereFois = (bool)dataReader["firstParticipation"];
                            string photo = dataReader["photo"].ToString();

                            Personne personne = new Personne(id, nom, email, tel, prenom, sexe, dateNaiss, poste, adressePostale, codePostal, ville, commentaire, isPayed, taille, regime, premiereFois, photo);
                            listPersonne.Add(personne);
                        }

                        //close Data Reader
                        dataReader.Close();

                        //close Connection
                        this.CloseConnection();

                        //return list to be displayed
                        return listPersonne;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return listPersonne;
        }

        public List<Personne> GetAllPersonneByEventAndPoste(string evenement, string post)
        {
            List<Personne> listPersonne = new List<Personne>();
            try
            {
                if (!string.IsNullOrEmpty(evenement) && !string.IsNullOrEmpty(post))
                {
                    listPersonne = GetAllPersonneByEvent(evenement.Trim());
                    return listPersonne.Where(x => x.poste.Equals(post.Trim(), StringComparison.InvariantCultureIgnoreCase)).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return listPersonne;
        }

        public List<string> GetAllMemberByEventAndEdition(string evenement, string anneeEdition, out string message, out List<Personne> listMemberInCsv)
        {
            message = string.Empty;
            listMemberInCsv = new List<Personne>();
            List<string> data = new List<string>();
            try
            {
                if(!string.IsNullOrEmpty(evenement) && !string.IsNullOrEmpty(anneeEdition))
                {
                    if(OpenConnection())
                    {
                        string queryCheckEventWithEdition = string.Format("select count(*) from evenement ev join edition ed on (ev.idEdition = ed.id) where nom = '{0}' and ed.annee = '{1}'",
                            evenement, anneeEdition);
                        MySqlCommand cmdCheckEventWithEdition = new MySqlCommand(queryCheckEventWithEdition, connection);
                        int Count = int.Parse(cmdCheckEventWithEdition.ExecuteScalar() + "");
                        if(Count == 1 || (evenement == "Aucun" && anneeEdition == "Aucune"))
                        {
                            string queryGetAllMember;
                            if (evenement == "Aucun" && anneeEdition == "Aucune")
                            {
                                queryGetAllMember = string.Format("select * from personne where idEvenement IS NULL order by id");
                            }
                            else
                            {
                                queryGetAllMember = string.Format("select * from personne p join evenement ev on (p.idEvenement = ev.id) join edition ed on (ev.idEdition = ed.id) where ev.nom = '{0}' and ed.annee = '{1}' order by p.id", evenement, anneeEdition);
                            }
                            
                            MySqlCommand cmdGetAllmember = new MySqlCommand(queryGetAllMember, connection);
                            //Create a data reader and Execute the command
                            MySqlDataReader dataReader = cmdGetAllmember.ExecuteReader();

                            //Read the data and store them in the list
                            while (dataReader.Read())
                            {
                                string id = dataReader["id"].ToString();
                                string nom = dataReader["nom"].ToString();
                                string prenom = dataReader["prenom"].ToString();
                                string email = dataReader["adrMail"].ToString();
                                string tel = dataReader["numTel"].ToString();
                                string sexe = dataReader["sexe"].ToString();
                                string dateNaiss = dataReader["dateNaissance"].ToString();
                                string poste = dataReader["poste"].ToString();
                                string adressePostale = dataReader["adrPost"].ToString();
                                string codePostal = dataReader["codePostal"].ToString();
                                string ville = dataReader["ville"].ToString();
                                string commentaire = dataReader["commentaire"].ToString();
                                string isPayed = dataReader["isPayed"].ToString();
                                string taille = dataReader["taille"].ToString();
                                string regime = dataReader["regimeAlimentaire"].ToString();
                                string premiereFois = dataReader["firstParticipation"].ToString();
                                string photo = dataReader["photo"].ToString();

                                string dataMember = id + ";" + nom + ";" + prenom + ";" + email + ";" + tel 
                                    + ";" + sexe + ";" + dateNaiss + ";" + poste + ";" + adressePostale 
                                    + ";" + codePostal + ";" + ville + ";" + commentaire + ";" + isPayed + ";" 
                                    + taille + ";" + regime + ";" + premiereFois + ";" + photo;
                                data.Add(dataMember);
                                listMemberInCsv.Add(new Personne(nom, prenom, email));
                            }
                            if(data.Count > 0)
                            {
                                message = "Success ! " + data.Count + " members added to the Csv file. This file will now begin to download.";
                            }
                            else
                            {
                                message = "No members are subscribed at this event.";
                            }
                            dataReader.Close();
                            CloseConnection();

                            
                            return data;
                        }
                        else
                        {
                            message = string.Format("Aucun �v�nement nomm� {0} en {1} ne se trouve dans la base de donn�es");
                            return data;
                        }
                    }
                    else
                    {
                        message = "Connexion � la base de donn�es impossible.";
                        return data;
                    }
                }
                else
                {
                    message = "Un des champs requis est vide ou null.";
                    return data;
                }
            }
            catch(Exception ex)
            {
                Log.Write(ex.Message);
            }
            return data;
        }

        public List<Evenement> GetAllEvent()
        {
            List<Evenement> listEvent = new List<Evenement>();
            CultureInfo provider = CultureInfo.InvariantCulture;
            string query = "select * from evenement ev join edition ed on ev.idEdition = ed.id order by ed.annee";
            try
            {
                if (OpenConnection() == true)
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        int id = int.Parse(dataReader["id"].ToString());
                        string nom = dataReader["nom"].ToString();
                        int codePostal = int.Parse(dataReader["codePostal"].ToString());
                        string ville = dataReader["ville"].ToString();
                        string pays = dataReader["pays"].ToString();
                        string rue = dataReader["rue"].ToString();
                        int numRue = int.Parse(dataReader["numRue"].ToString());
                        string dateDebutString = dataReader["debut"].ToString().Replace('-', '/');
                        string dateFinString = dataReader["fin"].ToString().Replace('-', '/');
                        string format = "dd/MM/yyyy HH:mm:ss";

                        DateTime dateDebut = DateTime.ParseExact(dateDebutString, format, provider);
                        DateTime dateFin = DateTime.ParseExact(dateFinString, format, provider);

                        Evenement evenement = new Evenement(id, nom, codePostal, ville, pays, rue, numRue, dateDebut, dateFin);
                        listEvent.Add(evenement);
                    }
                    dataReader.Close();
                    CloseConnection();
                    return listEvent;
                }
            }
            catch (Exception ex)
            {

            }
            return listEvent;
        }

        public List<Evenement> GetAllEventByName()
        {
            List<Evenement> listEvent = new List<Evenement>();
            CultureInfo provider = CultureInfo.InvariantCulture;
            string query = string.Format("select * from evenement group by nom order by nom");
            try
            {
                if (OpenConnection() == true)
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        int id = int.Parse(dataReader["id"].ToString());
                        string nom = dataReader["nom"].ToString();
                        int codePostal = int.Parse(dataReader["codePostal"].ToString());
                        string ville = dataReader["ville"].ToString();
                        string pays = dataReader["pays"].ToString();
                        string rue = dataReader["rue"].ToString();
                        int numRue = int.Parse(dataReader["numRue"].ToString());
                        string dateDebutString = dataReader["debut"].ToString().Replace('-', '/');
                        string dateFinString = dataReader["fin"].ToString().Replace('-', '/');
                        string format = "dd/MM/yyyy HH:mm:ss";

                        DateTime dateDebut = DateTime.ParseExact(dateDebutString, format, provider);
                        DateTime dateFin = DateTime.ParseExact(dateFinString, format, provider);

                        Evenement evenement = new Evenement(id, nom, codePostal, ville, pays, rue, numRue, dateDebut, dateFin);
                        listEvent.Add(evenement);
                    }
                    dataReader.Close();
                    CloseConnection();
                    return listEvent;
                }
            }
            catch (Exception ex)
            {

            }
            return listEvent;
        }

        public Evenement GetEvent(string idEvent)
        {
            Evenement evenement = new Evenement();
            CultureInfo provider = CultureInfo.InvariantCulture;
            try
            {
                if (!string.IsNullOrEmpty(idEvent))
                {
                    if (OpenConnection())
                    {
                        string queryCheckEvent = string.Format("select count(*) from evenement where id = {0}", idEvent.Trim());
                        MySqlCommand cmdCheckEvent = new MySqlCommand(queryCheckEvent, connection);
                        int Count = int.Parse(cmdCheckEvent.ExecuteScalar() + "");
                        if (Count == 1)
                        {
                            string queryGetEvent = string.Format("select count(*) from evenement where id = {0}", idEvent.Trim());
                            MySqlCommand cmdGetEvent = new MySqlCommand(queryGetEvent, connection);
                            MySqlDataReader dataReader = cmdGetEvent.ExecuteReader();
                            while (dataReader.Read())
                            {
                                int id = int.Parse(dataReader["id"].ToString());
                                string nom = dataReader["nom"].ToString();
                                int codePostal = int.Parse(dataReader["codePostal"].ToString());
                                string ville = dataReader["ville"].ToString();
                                string pays = dataReader["pays"].ToString();
                                string rue = dataReader["rue"].ToString();
                                int numRue = int.Parse(dataReader["numRue"].ToString());
                                string dateDebutString = dataReader["debut"].ToString().Replace('-', '/');
                                string dateFinString = dataReader["fin"].ToString().Replace('-', '/');
                                string format = "dd/MM/yyyy HH:mm:ss";

                                DateTime dateDebut = DateTime.ParseExact(dateDebutString, format, provider);
                                DateTime dateFin = DateTime.ParseExact(dateFinString, format, provider);

                                return new Evenement(id, nom, codePostal, ville, pays, rue, numRue, dateDebut, dateFin);
                            }
                            dataReader.Close();
                            CloseConnection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return evenement;
        }

        public bool EditEvent(Dictionary<string, string> dicoInfoEvent, out string message)
        {
            message = "";
            Evenement evenement = new Evenement();
            CultureInfo provider = CultureInfo.InvariantCulture;
            try
            {
                if (OpenConnection())
                {
                    string queryCheckEvent = string.Format("select count(*) from evenement where id = {0}", dicoInfoEvent["id"]);
                    MySqlCommand cmdCheckEvent = new MySqlCommand(queryCheckEvent, connection);
                    int Count = int.Parse(cmdCheckEvent.ExecuteScalar() + "");
                    if (Count == 1)
                    {
                        dicoInfoEvent["startEvent"] = dicoInfoEvent["startEvent"].Replace('/', '-');
                        dicoInfoEvent["endEvent"] = dicoInfoEvent["endEvent"].Replace('/', '-');

                        DateTime start = DateTime.ParseExact(dicoInfoEvent["startEvent"], "dd-MM-yyyy H:m", CultureInfo.InvariantCulture);
                        DateTime end = DateTime.ParseExact(dicoInfoEvent["endEvent"], "dd-MM-yyyy H:m", CultureInfo.InvariantCulture);
                        string startEvent = start.ToString("yyyy-MM-dd HH:mm:ss");
                        string endEvent = end.ToString("yyyy-MM-dd HH:mm:ss");

                        string queryEditEvent = string.Format("UPDATE evenement SET nom='{0}', numRue='{1}', rue='{2}', ville='{3}', codePostal='{4}', pays='{5}', debut='{6}', fin='{7}' WHERE id='{8}'", dicoInfoEvent["name"], dicoInfoEvent["numberStreet"], dicoInfoEvent["nameStreet"], dicoInfoEvent["city"], dicoInfoEvent["postalCode"], dicoInfoEvent["country"], startEvent, endEvent, dicoInfoEvent["id"]);
                        MySqlCommand cmdEditEvent = new MySqlCommand(queryEditEvent, connection);
                        cmdEditEvent.ExecuteNonQuery();
                        this.CloseConnection();
                        message = "L'evenement a bien �t� modifi�.";
                        return true;
                    }
                    else
                    {
                        message = "Bizarre, l'�v�nement ne semble pas exister.";
                    }
                }
                else
                {
                    message = "Impossible d'acceder � la base de donn�es.";
                }
            }
            catch (Exception ex)
            {
                message = "Impossible d'�diter l'�v�nement. Un champ est vide ou invalide.";
                Log.Write(ex.Message);
                return false;
            }
            return false;
        }

        public string GetAjaxEvent(string idEvent)
        {
            Evenement evenement = new Evenement();
            CultureInfo provider = CultureInfo.InvariantCulture;
            try
            {
                if (!string.IsNullOrEmpty(idEvent))
                {
                    if (OpenConnection())
                    {
                        string queryCheckEvent = string.Format("select count(*) from evenement where id = {0}", idEvent.Trim());
                        MySqlCommand cmdCheckEvent = new MySqlCommand(queryCheckEvent, connection);
                        int Count = int.Parse(cmdCheckEvent.ExecuteScalar() + "");
                        if (Count == 1)
                        {
                            string queryGetEvent = string.Format("select * from evenement where id = {0}", idEvent.Trim());
                            MySqlCommand cmdGetEvent = new MySqlCommand(queryGetEvent, connection);
                            MySqlDataReader dataReader = cmdGetEvent.ExecuteReader();
                            while (dataReader.Read())
                            {
                                int id = int.Parse(dataReader["id"].ToString());
                                string nom = dataReader["nom"].ToString();
                                int codePostal = int.Parse(dataReader["codePostal"].ToString());
                                string ville = dataReader["ville"].ToString();
                                string pays = dataReader["pays"].ToString();
                                string rue = dataReader["rue"].ToString();
                                int numRue = int.Parse(dataReader["numRue"].ToString());
                                string dateDebutString = dataReader["debut"].ToString().Replace('-', '/');
                                string dateFinString = dataReader["fin"].ToString().Replace('-', '/');
                                string format = "dd/MM/yyyy HH:mm:ss";

                                DateTime dateDebut = DateTime.ParseExact(dateDebutString, format, provider);
                                DateTime dateFin = DateTime.ParseExact(dateFinString, format, provider);
                                CloseConnection();
                                return new JavaScriptSerializer().Serialize(new Evenement(nom, codePostal, ville, pays, rue, numRue, dateDebut, dateFin));
                            }
                            dataReader.Close();
                            CloseConnection();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return new JavaScriptSerializer().Serialize(evenement);
        }

        public string GetAllAjaxEvent()
        {
            Evenement evenement = new Evenement();
            CultureInfo provider = CultureInfo.InvariantCulture;
            var jsonSerialiser = new JavaScriptSerializer();
            List<Evenement> listEvent = new List<Evenement>();
            try
            {

                if (OpenConnection())
                {

                    string queryGetEvent = string.Format("select * from evenement group by nom");
                    MySqlCommand cmdGetEvent = new MySqlCommand(queryGetEvent, connection);
                    MySqlDataReader dataReader = cmdGetEvent.ExecuteReader();
                    while (dataReader.Read())
                    {
                        int id = int.Parse(dataReader["id"].ToString());
                        string nom = dataReader["nom"].ToString();
                        int codePostal = int.Parse(dataReader["codePostal"].ToString());
                        string ville = dataReader["ville"].ToString();
                        string pays = dataReader["pays"].ToString();
                        string rue = dataReader["rue"].ToString();
                        int numRue = int.Parse(dataReader["numRue"].ToString());
                        string dateDebutString = dataReader["debut"].ToString().Replace('-', '/');
                        string dateFinString = dataReader["fin"].ToString().Replace('-', '/');
                        string format = "dd/MM/yyyy HH:mm:ss";

                        DateTime dateDebut = DateTime.ParseExact(dateDebutString, format, provider);
                        DateTime dateFin = DateTime.ParseExact(dateFinString, format, provider);
                        listEvent.Add(new Evenement(id, nom, codePostal, ville, pays, rue, numRue, dateDebut, dateFin));
                    }
                    dataReader.Close();
                    CloseConnection();
                    return jsonSerialiser.Serialize(listEvent);
                }
            }
            catch (Exception ex)
            {

            }
            return new JavaScriptSerializer().Serialize(listEvent);
        }

        public string GetJsonAssociation()
        {
            Association association = new Association();
            var jsonSerialiser = new JavaScriptSerializer();

            List<Association> allAssociation = new List<Association>();
            try
            {
                string queryGetAssociation = string.Format("select id, nom from association");
                if (OpenConnection())
                {
                    MySqlCommand cmdGetAssociation = new MySqlCommand(queryGetAssociation, connection);
                    MySqlDataReader dataReader = cmdGetAssociation.ExecuteReader();
                    while (dataReader.Read())
                    {
                        int id = int.Parse(dataReader["id"].ToString());
                        string nom = dataReader["nom"].ToString();
                        allAssociation.Add(new Association(id, nom));
                    }
                    dataReader.Close();
                    CloseConnection();
                    return jsonSerialiser.Serialize(allAssociation);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return "";
        }

        public string GetJsonEdition(string nomEvent)
        {
            var jsonSerialiser = new JavaScriptSerializer();
            List<Edition> listEdition = new List<Edition>();
            try
            {
                if (!string.IsNullOrEmpty(nomEvent))
                {
                    if (OpenConnection())
                    {
                        string queryGetEvent = string.Format("select * from edition join evenement on evenement.idEdition = edition.id where evenement.nom = '{0}'", nomEvent.Trim());
                        MySqlCommand cmdGetEvent = new MySqlCommand(queryGetEvent, connection);
                        MySqlDataReader dataReader = cmdGetEvent.ExecuteReader();
                        while (dataReader.Read())
                        {
                            int id = int.Parse(dataReader["id"].ToString());
                            int annee = int.Parse(dataReader["annee"].ToString());
                            listEdition.Add(new Edition(id, annee));
                        }
                        dataReader.Close();
                        CloseConnection();
                        return jsonSerialiser.Serialize(listEdition);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return jsonSerialiser.Serialize(listEdition);
        }

        public string GetJsonGroupe(string idEdition, string nomEvent)
        {
            // TODO FIX IdEvent est toujours � 1 donc regler �a autrement
            var jsonSerialiser = new JavaScriptSerializer();
            List<Groupe> listGroupe = new List<Groupe>();
            try
            {
                if (!string.IsNullOrEmpty(idEdition) && !string.IsNullOrEmpty(nomEvent))
                {
                    if (OpenConnection())
                    {
                        string queryGetGroupe = string.Format("SELECT * FROM groupe g join evenement ev on g.idEvenement = ev.id join edition ed on ed.id = ev.idEdition where ev.nom = '{0}' and ed.id = '{1}' ", nomEvent, idEdition);
                        MySqlCommand cmdGetGroupe = new MySqlCommand(queryGetGroupe, connection);
                        MySqlDataReader dataReader = cmdGetGroupe.ExecuteReader();
                        while (dataReader.Read())
                        {
                            int id = int.Parse(dataReader["id"].ToString());
                            string nomGroupe = dataReader["nomGroupe"].ToString();
                            int idEvenement = int.Parse(dataReader["idEvenement"].ToString());
                            listGroupe.Add(new Groupe(id, nomGroupe, idEvenement));
                        }
                        dataReader.Close();
                        CloseConnection();
                        return jsonSerialiser.Serialize(listGroupe);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return jsonSerialiser.Serialize(listGroupe);
        }

        public List<Groupe> GetAllGroup()
        {
            List<Groupe> listGroupe = new List<Groupe>();
            string query = "select * from groupe order by id";
            try
            {
                if (OpenConnection() == true)
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    //Read the data and store them in the list
                    while (dataReader.Read())
                    {
                        int id = int.Parse(dataReader["id"].ToString());
                        string nomGroupe = dataReader["nomGroupe"].ToString();
                        int idEvenement = int.Parse(dataReader["idEvenement"].ToString());

                        Groupe groupe = new Groupe(id, nomGroupe, idEvenement);
                        listGroupe.Add(groupe);
                    }

                    //close Data Reader
                    dataReader.Close();

                    //close Connection
                    this.CloseConnection();

                    //return list to be displayed
                    return listGroupe;
                }
            }
            catch (Exception ex)
            {
                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return listGroupe;
            }
            return listGroupe;
        }

        public List<Evenement> SearchEvent(string input)
        {
            List<Evenement> listEvent = new List<Evenement>();
            CultureInfo provider = CultureInfo.InvariantCulture;
            string queryGetEventSearch = string.Format("select * from evenement ev join edition ed on ev.idEdition = ed.id where ev.nom like '%{0}%' or ev.ville like '%{0}%' or ev.numRue like '%{0}%' or ev.rue like '%{0}%' or ev.pays like '%{0}%' or ev.debut like '%{0}%' or ev.fin like '%{0}%' order by ed.annee", input);

            try
            {
                if (OpenConnection() == true)
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(queryGetEventSearch, connection);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader = cmd.ExecuteReader();

                    while (dataReader.Read())
                    {
                        int id = int.Parse(dataReader["id"].ToString());
                        string nom = dataReader["nom"].ToString();
                        int codePostal = int.Parse(dataReader["codePostal"].ToString());
                        string ville = dataReader["ville"].ToString();
                        string pays = dataReader["pays"].ToString();
                        string rue = dataReader["rue"].ToString();
                        int numRue = int.Parse(dataReader["numRue"].ToString());
                        string dateDebutString = dataReader["debut"].ToString().Replace('-', '/');
                        string dateFinString = dataReader["fin"].ToString().Replace('-', '/');
                        string format = "dd/MM/yyyy HH:mm:ss";

                        DateTime dateDebut = DateTime.ParseExact(dateDebutString, format, provider);
                        DateTime dateFin = DateTime.ParseExact(dateFinString, format, provider);

                        Evenement evenement = new Evenement(id, nom, codePostal, ville, pays, rue, numRue, dateDebut, dateFin);
                        listEvent.Add(evenement);
                    }
                    dataReader.Close();
                    CloseConnection();
                    return listEvent;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return listEvent;
        }

        public Dictionary<string, int> GetAllTaille(string evenement)
        {
            Dictionary<string, int> allTaille = new Dictionary<string, int>();
            string queryNomTaille = "select taille from personne p join evenement e on p.idEvenement = e.id where e.nom = '" + evenement + "' group by taille";
            try
            {
                if (!string.IsNullOrEmpty(evenement))
                {
                    if (OpenConnection() == true)
                    {
                        MySqlCommand cmd = new MySqlCommand(queryNomTaille, connection);
                        MySqlDataReader dataReader = cmd.ExecuteReader();

                        List<string> tailles = new List<string>();
                        while (dataReader.Read())
                        {
                            tailles.Add(dataReader["taille"].ToString());

                        }
                        dataReader.Close();
                        foreach (var taille in tailles)
                        {
                            string queryCountTaille = "select count(*) from personne p join evenement e on p.idEvenement = e.id ";
                            queryCountTaille += "where e.nom = '" + evenement + "' and p.taille = '" + taille + "'";
                            MySqlCommand cmdCountTaille = new MySqlCommand(queryCountTaille, connection);
                            int Count = int.Parse(cmdCountTaille.ExecuteScalar() + "");
                            allTaille.Add(taille, Count);
                        }
                        CloseConnection();
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return allTaille;
        }

        public bool AddEvent(Evenement e, out string message)
        {
            message = "";
            try
            {
                int yearNow = DateTime.Now.Year;
                string start = e.dateDebut.ToString("yyyy-MM-dd HH:mm:ss");
                string end = e.dateFin.ToString("yyyy-MM-dd HH:mm:ss");

                if (!string.IsNullOrEmpty(e.nom) && !string.IsNullOrEmpty(e.pays) && !string.IsNullOrEmpty(e.ville)
                    && !string.IsNullOrEmpty(e.rue))
                //e.dateDebut >= DateTime.Now && e.dateFin > e.dateDebut
                {
                    string queryCheckEvent = string.Format("select count(*) from Evenement where nom = '{0}' and debut = '{1}'", e.nom, start);
                    if (OpenConnection())
                    {
                        MySqlCommand cmdCheckEvent = new MySqlCommand(queryCheckEvent, connection);
                        int eventAlreadyExist = int.Parse(cmdCheckEvent.ExecuteScalar() + "");
                        if (eventAlreadyExist == 0)
                        {
                            // Check Edition
                            string queryCheckEdition = string.Format("select count(*) from edition where annee = {0}", e.annee);
                            MySqlCommand cmdCheckEdition = new MySqlCommand(queryCheckEdition, connection);
                            int editionAlreadyExist = int.Parse(cmdCheckEdition.ExecuteScalar() + "");
                            if (editionAlreadyExist == 0)
                            {
                                // Add Edition
                                string queryAddEdition = string.Format("insert into edition (annee) values ({0})", e.annee);
                                MySqlCommand cmdAddEdition = new MySqlCommand(queryAddEdition, connection);
                                cmdAddEdition.ExecuteNonQuery();
                            }

                            // Get Id Edition
                            string querySelectIdEdition = string.Format("select id from edition where annee = {0}", e.annee);
                            MySqlCommand cmdGetIdEdition = new MySqlCommand(querySelectIdEdition, connection);
                            MySqlDataReader dataReader = cmdGetIdEdition.ExecuteReader();
                            if (!dataReader.Read())
                                throw new InvalidOperationException("No records were returned.");

                            int idEdition = dataReader.GetInt32("id");

                            if (dataReader.Read())
                                throw new InvalidOperationException("Multiple records were returned.");
                            dataReader.Dispose();
                            dataReader.Close();


                            // Add Event
                            string queryAddEvent = string.Format("insert into evenement(nom, pays, ville, codePostal, rue, numRue, debut, fin, idEdition) values ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')"
                                , e.nom, e.pays, e.ville, e.codePostal, e.rue, e.numRue, start, end, idEdition);
                            MySqlCommand cmdAddEvent = new MySqlCommand(queryAddEvent, connection);
                            cmdAddEvent.ExecuteNonQuery();
                            CloseConnection();
                            return true;

                        }
                        else
                        {

                            message = "This event already exist.";
                            
                        }
                        CloseConnection();
                        return false;
                    }
                    else
                    {
                        message = "Can not to access the OpenConnection function.";
                        return false;
                    }
                }
                else
                {
                    message = "A field is missing";
                    return false;
                }

            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return false;
        }

        public bool AddPersonne(string nom, string prenom, string mail, string tel, string sexe,
            DateTime dateNaiss, string poste, string adressePostale, int codePostal, string ville,
            string commentaire, bool isPayed, string taille, string regime, bool premiereFois,
            string photo, string groupe, string evenement, string reglement, string association)
        {
            DateTime now = DateTime.Now;
            int ageLimite = 5;
            try
            {
                if (!string.IsNullOrEmpty(nom) && !string.IsNullOrEmpty(prenom) && !string.IsNullOrEmpty(mail) && !string.IsNullOrEmpty(evenement))
                {
                    string queryCheckPersonne = string.Format("select Count(*) from Personne where adrMail = '{0}' and prenom = '{1}' and nom = '{2}'", mail.Trim(), prenom.Trim(), nom.Trim());
                    if (OpenConnection())
                    {
                        int idGroupe = 0;
                        int idAssociation = 0;
                        int payed = 0;
                        int firstTime = 0;
                        MySqlCommand cmd = new MySqlCommand(queryCheckPersonne, connection);
                        int PersonneAlreadyExist = int.Parse(cmd.ExecuteScalar() + "");
                        if (PersonneAlreadyExist == 0)
                        {
                            if (string.IsNullOrEmpty(tel)) { tel = string.Empty; }
                            if (string.IsNullOrEmpty(sexe)) { sexe = string.Empty; }
                            if (dateNaiss == null || dateNaiss.Year < now.Year - 100 || dateNaiss.Year >= now.Year - ageLimite) { dateNaiss = new DateTime(); }
                            if (string.IsNullOrEmpty(poste)) { poste = string.Empty; }
                            if (string.IsNullOrEmpty(adressePostale)) { adressePostale = string.Empty; }
                            if (codePostal < 00001 || codePostal > 99999) { codePostal = 00000; }
                            if (string.IsNullOrEmpty(ville)) { ville = string.Empty; }
                            if (string.IsNullOrEmpty(commentaire)) { commentaire = string.Empty; }
                            if (isPayed) { payed = 1; }
                            if (premiereFois) { firstTime = 1; }
                            if (string.IsNullOrEmpty(taille)) { taille = string.Empty; }
                            if (string.IsNullOrEmpty(regime)) { regime = string.Empty; }
                            if (string.IsNullOrEmpty(photo)) { photo = "~/Contents/Images/unknownUser.png"; }
                            if (string.IsNullOrEmpty(groupe)) { groupe = string.Empty; }
                            if (string.IsNullOrEmpty(reglement)) { reglement = string.Empty; }
                            if (string.IsNullOrEmpty(association)) { association = string.Empty; }

                            string queryCheckAssociation = string.Format("select Count(*) from Association where nom = '{0}'", association.Trim());
                            cmd = new MySqlCommand(queryCheckAssociation, connection);
                            int AssociationAlreadyExist = int.Parse(cmd.ExecuteScalar() + "");
                            if (AssociationAlreadyExist == 0 && !string.IsNullOrEmpty(association))
                            {
                                string queryAddAssociation = string.Format("insert into association(nom) values('{0}')", association.Trim());
                                MySqlCommand cmdAddAssociation = new MySqlCommand(queryAddAssociation, connection);
                                cmdAddAssociation.ExecuteNonQuery();
                            }
                            if (!string.IsNullOrEmpty(association))
                            {
                                string GetIdAssociation = string.Format("select id from association where nom = '{0}'", association);
                                cmd = new MySqlCommand(GetIdAssociation, connection);
                                idAssociation = int.Parse(cmd.ExecuteScalar() + "");
                            }


                            string queryGetIdEvenement = string.Format("select id from evenement where nom = '{0}'", evenement);
                            cmd = new MySqlCommand(queryGetIdEvenement, connection);
                            int idEvent = int.Parse(cmd.ExecuteScalar() + "");

                            string queryCheckGroupe = string.Format("select Count(*) from groupe where nomGroupe = '{0}'", groupe.Trim());
                            cmd = new MySqlCommand(queryCheckGroupe, connection);
                            int GroupeAlreadyExist = int.Parse(cmd.ExecuteScalar() + "");
                            if (GroupeAlreadyExist == 0 && !string.IsNullOrEmpty(groupe) && idEvent > 0)
                            {
                                string queryAddAssociation = string.Format("insert into Groupe(nomGroupe, idEvenement) values('{0}', '{1}')", groupe.Trim(), idEvent);
                                MySqlCommand cmdAddAssociation = new MySqlCommand(queryAddAssociation, connection);
                                cmdAddAssociation.ExecuteNonQuery();
                            }
                            if (!string.IsNullOrEmpty(groupe) && idEvent > 0)
                            {
                                string GetIdGroupe = string.Format("select id from groupe where nomGroupe = '{0}' and idEvenement = '{1}'", groupe, idEvent);
                                cmd = new MySqlCommand(GetIdGroupe, connection);
                                idGroupe = int.Parse(cmd.ExecuteScalar() + "");
                            }

                            //Add Personne
                            string queryAddPersonne = string.Format("insert into Personne(nom, prenom, sexe, dateNaissance, poste, adrMail, numTel, adrPost, codePostal");
                            queryAddPersonne += string.Format(", ville, commentaire, isPayed, taille, regimeAlimentaire, firstParticipation, photo, idAssociation, idGroupe, idEvenement) ");
                            queryAddPersonne += string.Format("Values('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}')",
                                nom.Trim(), prenom.Trim(), sexe.Trim(), dateNaiss, poste.Trim(), mail.Trim(), tel, adressePostale.Trim(), codePostal,
                                ville.Trim(), commentaire.Trim(), payed, taille.Trim(), regime.Trim(), firstTime, photo.Trim(), idAssociation, idGroupe, idEvent);
                            MySqlCommand cmdAddPersonne = new MySqlCommand(queryAddPersonne, connection);
                            cmdAddPersonne.ExecuteNonQuery();
                            this.CloseConnection();
                            return true;
                        }
                        this.CloseConnection();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return false;
        }

        public bool AddPersonne(Personne p, string association, string evenement, string edition, string groupe, out string message)
        {
            string idEvent = "NULL";
            string idEdition = "NULL";
            string idGroupe = "NULL";
            string idAssociation = "NULL";
            string dateNaiss = p.dateNaissance.ToString("yyyy-MM-dd");

            message = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(p.nom) && !string.IsNullOrEmpty(p.prenom) && !string.IsNullOrEmpty(p.email)
                    && !string.IsNullOrEmpty(evenement) && !string.IsNullOrEmpty(edition))
                {
                    if (OpenConnection())
                    {


                        string queryGetIdEvent = string.Format("select * from evenement ev join edition ed on (ev.idEdition = ed.id) where ev.nom = '{0}' and ed.annee = '{1}'", evenement, edition);
                        MySqlCommand cmdGetIdEvent = new MySqlCommand(queryGetIdEvent, connection);
                        MySqlDataReader dataReader = cmdGetIdEvent.ExecuteReader();
                        while (dataReader.Read())
                        {
                            idEvent = dataReader["id"].ToString();
                        }
                        dataReader.Close();
                        dataReader.Dispose();

                        string queryGetIdEdition = string.Format("select id from edition where annee = '{0}'", edition);
                        MySqlCommand cmdGetIdEdition = new MySqlCommand(queryGetIdEdition, connection);
                        MySqlDataReader dataReader2 = cmdGetIdEdition.ExecuteReader();
                        while (dataReader2.Read())
                        {
                            idEdition = dataReader2["id"].ToString();
                        }
                        dataReader2.Close();
                        dataReader2.Dispose();

                        if (!string.IsNullOrEmpty(groupe))
                        {
                            string queryGetIdGroupe = string.Format("select * from groupe g join evenement e on (g.idEvenement = e.id) where nomGroupe = '{0}'", groupe);
                            MySqlCommand cmdGetIdGroupe = new MySqlCommand(queryGetIdGroupe, connection);
                            MySqlDataReader dataReader3 = cmdGetIdGroupe.ExecuteReader();
                            while (dataReader3.Read())
                            {
                                idGroupe = dataReader3["id"].ToString();
                            }
                            dataReader3.Close();
                            dataReader3.Dispose();
                        }

                        if (!string.IsNullOrEmpty(association))
                        {
                            string queryGetIdAssociation = string.Format("select id from association where nom = '{0}'", association);
                            MySqlCommand cmdGetIdAssociation = new MySqlCommand(queryGetIdAssociation, connection);
                            MySqlDataReader dataReader4 = cmdGetIdAssociation.ExecuteReader();
                            while (dataReader4.Read())
                            {
                                idAssociation = dataReader4["id"].ToString();
                            }
                            dataReader4.Close();
                            dataReader4.Dispose();
                        }

                        string queryCheckPersonne = string.Format("select count(*) from personne join evenement on (personne.idEvenement = evenement.id) join edition on (evenement.idEdition = edition.id) where ((personne.nom = '{0}' and personne.prenom = '{1}') or personne.adrMail = '{2}') and personne.idEvenement = {3}",
                            p.nom, p.prenom, p.email, idEvent);
                        MySqlCommand cmdCheckPersonne = new MySqlCommand(queryCheckPersonne, connection);
                        int count = int.Parse(cmdCheckPersonne.ExecuteScalar() + "");
                        if (count == 0)
                        {
                            string queryAddMembre = string.Format("INSERT INTO `personne` (idEvenement, idGroupe, idAssociation, `nom`, `prenom`, `sexe`, `dateNaissance`, `poste`, `adrMail`, `numTel`, `adrPost`, `codePostal`, `ville`, `commentaire`, `isPayed`, `taille`, `regimeAlimentaire`, `firstParticipation`, `photo`) VALUES ({0}, {1}, {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, '{10}', {11}, '{12}', '{13}', {14}, '{15}', '{16}', {17}, '{18}')",
                            idEvent, idGroupe, idAssociation, p.nom, p.prenom, p.sexe, dateNaiss, p.poste, p.email, p.telephone, p.adressePostale, p.codePostal, p.ville, p.commentaire, p.isPayed, p.taille, p.regime, p.premiereFois, p.photo);
                            MySqlCommand cmdAddMembre = new MySqlCommand(queryAddMembre, connection);
                            cmdAddMembre.ExecuteNonQuery();
                            CloseConnection();
                            return true;
                        }
                        else
                        {
                            message = "Cet utilisateur existe d�j� dans la base de donn�e";
                            CloseConnection();
                            return false;
                        }
                    }
                    else
                    {
                        message = "Acc�s impossible � la base de donn�e";
                        return false;
                    }
                }
                else
                {
                    message = "Un des champs obligatoire est necessaire pour ajouter ce membre";
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
                message = ex.Message;
                return false;
            }
            return false;
        }

        public bool AddPersonneByCsvFormat(List<string> allLines, out string message, out List<Personne> listMemberAdded, out List<Personne> listMemberNotAdded)
        {
            message = "";
            List<string> allMemberToAdd = new List<string>();
            listMemberAdded = new List<Personne>();
            listMemberNotAdded = new List<Personne>();
            CultureInfo provider = CultureInfo.InvariantCulture;
            try
            {
                if (OpenConnection())
                {
                    string last = allLines.Last();
                    foreach (var line in allLines)
                    {
                        //Get all data
                        string name = string.IsNullOrEmpty(line.Split(';')[1]) ? "" : line.Split(';')[1].Replace('"', ' ').Trim();
                        string surname = string.IsNullOrEmpty(line.Split(';')[2]) ? "" : line.Split(';')[2].Replace('"', ' ').Trim();
                        string dateNaiss = string.IsNullOrEmpty(line.Split(';')[3]) ? "NULL" : line.Split(';')[3].Replace('"', ' ').Trim();
                        string adresse = string.IsNullOrEmpty(line.Split(';')[4]) ? "NULL" : line.Split(';')[4].Replace('"', ' ').Trim();
                        string tel = string.IsNullOrEmpty(line.Split(';')[5]) ? "NULL" : line.Split(';')[5].Replace('"', ' ').Trim();
                        string ville = string.IsNullOrEmpty(line.Split(';')[6]) ? "NULL" : line.Split(';')[6].Replace('"', ' ').Trim();
                        string cp = string.IsNullOrEmpty(line.Split(';')[7]) ? "NULL" : line.Split(';')[7].Replace('"', ' ').Trim();
                        string email = string.IsNullOrEmpty(line.Split(';')[8]) ? "" : line.Split(';')[8].Replace('"', ' ').Trim();
                        string poste = string.IsNullOrEmpty(line.Split(';')[9]) ? "NULL" : line.Split(';')[9].Replace('"', ' ').Trim();
                        string premiereFois = string.IsNullOrEmpty(line.Split(';')[10]) ? "NULL" : line.Split(';')[10].Replace('"', ' ').Trim();
                        string commentaire = string.IsNullOrEmpty(line.Split(';')[11]) ? "NULL" : line.Split(';')[11].Replace('"', ' ').Trim();
                        string inscription = string.IsNullOrEmpty(line.Split(';')[12]) ? "NULL" : line.Split(';')[12].Replace('"', ' ').Trim();
                        //--------------------------------------


                        //Parse all data
                        if (dateNaiss.Contains("/"))
                        {
                            dateNaiss = DateTime.ParseExact(dateNaiss, "dd/MM/yyyy", provider).ToString("yyyy-MM-dd");
                        }
                        else if (dateNaiss.Contains("-"))
                        {
                            dateNaiss = DateTime.ParseExact(dateNaiss, "dd-MM-yyyy", provider).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            message = "La date de naissance \"" + dateNaiss + "\" est dans le mauvais format.";
                            return false;
                        }

                        if (inscription.Contains("/"))
                        {
                            inscription = DateTime.ParseExact(inscription, "dd/MM/yyyy", provider).ToString("yyyy-MM-dd");
                        }
                        else if (inscription.Contains("-"))
                        {
                            inscription = DateTime.ParseExact(inscription, "dd-MM-yyyy", provider).ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            message = "La date d'inscription \"" + inscription + "\" est dans le mauvais format.";
                            return false;
                        }

                        cp = !string.IsNullOrEmpty(cp) ? cp : "NULL";
                        premiereFois = !string.IsNullOrEmpty(premiereFois) ? (premiereFois == "oui" ? premiereFois = "1" : premiereFois = "0") : premiereFois = "0";
                        //--------------------------------------

                        string queryCheckPersonne = string.Format("select count(*) from personne where nom = '{0}' and prenom = '{1}' and adrMail = '{2}'", name, surname, email);
                        MySqlCommand cmdCheckPersonne = new MySqlCommand(queryCheckPersonne, connection);
                        int Count = int.Parse(cmdCheckPersonne.ExecuteScalar() + "");
                        if (Count == 0)
                        {
                            allMemberToAdd.Add("(\"" + name + "\", \"" + surname + "\", \"" + email + "\", \"" + tel + "\", \"" + dateNaiss + "\", \"" + poste + "\", \"" + adresse + "\", \"" + cp + "\", \"" + ville + "\", \"" + commentaire + "\", \"0\", \"\" , \"" + premiereFois + "\", \"" + inscription + "\"),");
                            listMemberAdded.Add(new Personne(name, surname, email));
                        }
                        else
                        {
                            listMemberNotAdded.Add(new Personne(name, surname, email));
                        }
                    }
                    if (allMemberToAdd.Count > 0)
                    {
                        string queryAddPersonne = string.Format("insert into personne (nom, prenom, adrMail, numTel, dateNaissance, poste, adrPost, codePostal, ville, commentaire, isPayed, regimeAlimentaire, firstParticipation, inscription) values ");

                        foreach (string member in allMemberToAdd)
                        {
                            queryAddPersonne += string.Format(member);
                        }
                        // We remove the last "," of the string
                        queryAddPersonne = queryAddPersonne.Substring(0, queryAddPersonne.Length - 1);
                        MySqlCommand cmdAddPersonne = new MySqlCommand(queryAddPersonne, connection);
                        cmdAddPersonne.ExecuteNonQuery();
                        if (allMemberToAdd.Count < allLines.Count)
                        {
                            message = "Import successed ! " + allMemberToAdd.Count + " of " + allLines.Count + " user imported. The other user already exist.";
                        }
                        else
                        {
                            message = "Import successed ! " + allMemberToAdd.Count + " user imported.";
                        }

                        CloseConnection();
                        return true;
                    }
                    else
                    {
                        message = "All the user in the csv file already exists";
                        return false;
                    }
                }
                message = "We can't access to the database";
                return false;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                Log.Write(ex.Message);
            }
            return false;
        }

        public List<string> GetAllGroupeByEvent(string evenement)
        {
            List<string> listGroupe = new List<string>();
            string query = "select nomGroupe from groupe join evenement on groupe.idEvenement = evenement.id order by nomGroupe";
            try
            {
                if (OpenConnection() == true)
                {
                    //Create Command
                    MySqlCommand cmd = new MySqlCommand(query, connection);
                    //Create a data reader and Execute the command
                    MySqlDataReader dataReader = cmd.ExecuteReader();
                    while (dataReader.Read())
                    {
                        listGroupe.Add(dataReader["nomGroupe"].ToString());
                    }
                    dataReader.Close();
                    CloseConnection();
                    return listGroupe;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return listGroupe;
        }

        public bool HasPayed(string email, string evenement)
        {
            try
            {
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(evenement))
                {
                    string query = "SELECT Count(*) from personne p join evenement e on p.idEvenement = e.id ";
                    query += "where p.adrMail = '" + email.Trim() + "' and p.isPayed = 1";
                    if (OpenConnection() == true)
                    {
                        MySqlCommand cmd = new MySqlCommand(query, connection);
                        int Count = int.Parse(cmd.ExecuteScalar() + "");
                        this.CloseConnection();
                        if (Count == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
            return false;
        }



        #region ExempleUtilitaire
        //Insert statement
        public void Insert()
        {
            string query = "INSERT INTO tableinfo (name, age) VALUES('John Smith', '33')";

            //open connection
            if (this.OpenConnection() == true)
            {
                //create command and assign the query and connection from the constructor
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //Execute command
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Update statement
        public void Update()
        {
            string query = "UPDATE tableinfo SET name='Joe', age='22' WHERE name='John Smith'";

            //Open connection
            if (this.OpenConnection() == true)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = query;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }

        //Delete statement
        public void Delete()
        {
            string query = "DELETE FROM tableinfo WHERE name='John Smith'";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
        }

        //Select statement
        public List<string>[] Select()
        {
            string query = "SELECT * FROM tableinfo";

            //Create a list to store the result
            List<string>[] list = new List<string>[3];
            list[0] = new List<string>();
            list[1] = new List<string>();
            list[2] = new List<string>();

            //Open connection
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    list[0].Add(dataReader["id"] + "");
                    list[1].Add(dataReader["name"] + "");
                    list[2].Add(dataReader["age"] + "");
                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return list;
            }
            else
            {
                return list;
            }
        }

        //Count statement
        public int Count()
        {
            string query = "SELECT Count(*) FROM tableinfo";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                return Count;
            }
            else
            {
                return Count;
            }
        }

        //Backup
        public void Backup()
        {
            try
            {
                DateTime Time = DateTime.Now;
                int year = Time.Year;
                int month = Time.Month;
                int day = Time.Day;
                int hour = Time.Hour;
                int minute = Time.Minute;
                int second = Time.Second;
                int millisecond = Time.Millisecond;

                //Save file to C:\ with the current date as a filename
                string path;
                path = "C:\\" + year + "-" + month + "-" + day + "-" + hour + "-" + minute + "-" + second + "-" + millisecond + ".sql";
                StreamWriter file = new StreamWriter(path);


                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = "mysqldump";
                psi.RedirectStandardInput = false;
                psi.RedirectStandardOutput = true;
                psi.Arguments = string.Format(@"-u{0} -p{1} -h{2} {3}", uid, password, server, database);
                psi.UseShellExecute = false;

                Process process = Process.Start(psi);

                string output;
                output = process.StandardOutput.ReadToEnd();
                file.WriteLine(output);
                process.WaitForExit();
                file.Close();
                process.Close();
            }
            catch (IOException ex)
            {
                //MessageBox.Show("Error , unable to backup!");
            }
        }

        //Restore
        public void Restore()
        {
            try
            {
                //Read file from C:\
                string path;
                path = "C:\\MySqlBackup.sql";
                StreamReader file = new StreamReader(path);
                string input = file.ReadToEnd();
                file.Close();


                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = "mysql";
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = false;
                psi.Arguments = string.Format(@"-u{0} -p{1} -h{2} {3}", uid, password, server, database);
                psi.UseShellExecute = false;


                Process process = Process.Start(psi);
                process.StandardInput.WriteLine(input);
                process.StandardInput.Close();
                process.WaitForExit();
                process.Close();
            }
            catch (IOException ex)
            {
                //MessageBox.Show("Error , unable to Restore!");
            }
        }
        #endregion
    }
}
