﻿using E_vent;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace E_vent
{
    public partial class Login : PageBase
    {
        DBConnect db = new DBConnect();


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Log.CreateLogFile();
                if (isLogged())
                {
                    HttpContext.Current.Response.Redirect("~/Home.aspx");
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnForgotPassword_Click(object sender, EventArgs e)
        {
            panelForgotPassword.Visible = true;
            AuthenticationPanel.Visible = false;
        }

        protected void btnConnect_Click(object sender, EventArgs e)
        {
            //string login = inputLogin.Value.Trim().Protect();
            string login = inputLogin.Value.Trim();
            string login512 = GetSha512(Encrypt(login));
            string password512 = GetSha512(Encrypt(inputPassword.Value.Trim()));

            bool isLogged = db.IsAdmin(login512, password512);
            bool isAdmin = PageBase.isAdmin(login512, password512);

            if (isAdmin)
            {
                Login(login, password512, true);
            }
            else
            {
                if (isLogged)
                {
                    Login(login, password512, false);
                }
                else
                {
                    ShowMessage("Connexion impossible. Email ou mot de passe incorrect.", "Error");
                }
            }
        }



        protected void SendEmailForgotPassword_Click(object sender, EventArgs e)
        {
            string msgError = "";
            string email = inputLoginFP.Value.Trim();
            if (SendToken(email, out msgError))
            {
                ShowMessage("Un email de réinitialisation a bien été envoyé.", "Success");
            }
            else
            {
                ShowMessage(msgError, "Error");
            }
        }


        protected void ShowMessage(string Message, string typeErreur)
        {
            try
            {
                Console.Out.Write("debut");
                ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), @"ShowMessage('"+ Message  + "','"+ typeErreur.ToLower() + "')", true);
                Console.Out.Write("fin");
            }
            catch (Exception ex)
            {

            }

        }



        protected void btnBack_Click(object sender, ImageClickEventArgs e)
        {
            inputLoginFP.Value = string.Empty;
            panelForgotPassword.Visible = false;
            AuthenticationPanel.Visible = true;
        }
    }
}