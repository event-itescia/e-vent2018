﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_vent
{
    public class Edition
    {
        public int id { get; set; }
        public int annee { get; set; }

        public Edition()
        {

        }

        public Edition(int id, int annee)
        {
            this.id = id;
            this.annee = annee;
        }
    }
}