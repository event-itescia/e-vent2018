﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class CreateMember : PageBase
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            isAuthenticated();
        }

        protected void btnCreateMember_Click(object sender, EventArgs e)
        {
            try
            {
                string message = "";
                string nom = this.nom.Value.Trim();
                string prenom = this.prenom.Value.Trim();
                string sexePersonne = this.sexe.Value;
                string jourNaissance = jourNaiss.Value;
                string moisNaissance = this.moisNaissance.Value;
                string anneeNaissance = anneeNaiss.Value;
                string mail = this.mail.Value.Trim();
                string tel = this.tel.Value.Trim();
                string adresse = this.adresse.Value.Trim();
                string ville = this.ville.Value.Trim();
                int cp = (string.IsNullOrEmpty(this.cp.Value.Trim()) ? 0 : int.Parse(this.cp.Value.Trim())); // todo tryparse
                string commentaire = this.commentaire.Value.Trim();
                bool isPayed = this.isPayed.Value == "Oui" ? true : false;
                string taille = this.taille.Value;
                string regime = this.regime.Value;
                bool firstParticipation = this.firstParticipation.Value == "Oui" ? true : false;
                string poste = this.poste.Value;
                string association = (string.IsNullOrEmpty(associationValue.Value) ? null : associationValue.Value);
                string evenement = eventValue.Value;
                string edition = editionValue.Value;
                string groupe = (string.IsNullOrEmpty(groupeValue.Value) ? null : groupeValue.Value);
                string urlPhoto = photo.Value.Trim();

                int day = int.Parse(jourNaissance);
                int month = DateTimeFormatInfo.CurrentInfo.MonthNames.ToList().IndexOf(moisNaissance) + 1;
                int annee = int.Parse(anneeNaissance);
                DateTime dateNaiss = new DateTime(annee, month, day);


                if (evenement != "Aucun" && edition != "Aucune")
                {
                    Personne p = new Personne(nom, mail, tel, prenom, sexePersonne, dateNaiss, poste, adresse, cp, ville, commentaire, isPayed, taille, regime, firstParticipation, urlPhoto);
                    if(db.AddPersonne(p, association, evenement, edition, groupe, out message))
                    {
                        ShowMessage("Ajout terminé avec succès !", "success");
                    }
                    else
                    {
                        ShowMessage(message, "error");
                    }
                }
                else if (evenement == "Aucun" || edition == "Aucune")
                {
                    ShowMessage("Vous devez séléctionner un événemment et une édition.", "error");
                }
            }
            catch(Exception ex)
            {
                ShowMessage(@"Un des champs est peut-être mal renseigné. " + ex.Message, "error");
            }
            
        }

        #region WebMethod
        [System.Web.Services.WebMethod]
        public static string GetJsonAssociation()
        {
            DBConnect db2 = new DBConnect();
            string dataAssociation = db2.GetJsonAssociation();
            return dataAssociation;
        }

        [System.Web.Services.WebMethod]
        public static string GetJsonEvenement()
        {
            DBConnect db2 = new DBConnect();
            string dataEvent = db2.GetAllAjaxEvent();
            return dataEvent;
        }

        [System.Web.Services.WebMethod]
        public static string GetJsonEdition(string nomEvent)
        {
            DBConnect db2 = new DBConnect();
            string dataEdition = db2.GetJsonEdition(nomEvent);
            return dataEdition;
        }

        [System.Web.Services.WebMethod]
        public static string GetJsonGroupe(string idEdition, string nomEvenement)
        {
            DBConnect db2 = new DBConnect();
            string dataGroupe = db2.GetJsonGroupe(idEdition, nomEvenement);
            return dataGroupe;
        }
        #endregion
    }
}