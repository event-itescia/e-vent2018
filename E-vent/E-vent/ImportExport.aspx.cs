﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class ImportExport : PageBase
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            isAuthenticated();
            rptListEvent.DataSource = db.GetAllEventByName();
            rptListEvent.DataBind();

        }

        [System.Web.Services.WebMethod]
        public static string GetJsonEdition(string nomEvent)
        {
            DBConnect db2 = new DBConnect();
            string dataEdition = db2.GetJsonEdition(nomEvent);
            return dataEdition;
        }

        protected void btnImportCsv_Click(object sender, EventArgs e)
        {
            try
            {
                List<Personne> listMemberAdded = new List<Personne>();
                List<Personne> listMemberNotAdded = new List<Personne>();
                string message = "";
                List<string> allLines = new List<string>();

                if (inputCsv.HasFiles)
                {
                    string rootPath = Server.MapPath("~");
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFile file = Request.Files[i];

                        string path = rootPath + "App_Data\\" + file.FileName;
                        if (file.ContentLength > 0)
                        {
                            if (!File.Exists(path))
                            {
                                file.SaveAs(path);
                            }
                            allLines.AddRange(File.ReadLines(path, Encoding.Default).ToList());
                            File.Delete(path);
                        }

                    }
                    if (db.AddPersonneByCsvFormat(allLines, out message, out listMemberAdded, out listMemberNotAdded))
                    {
                        ShowMessage(message, "success");
                    }
                    else
                    {
                        ShowMessage(message, "error");
                    }
                }
                else
                {
                    ShowMessage("No csv file detected.", "error");
                }
                rptMemberAdded.DataSource = listMemberAdded;
                rptMemberNotAdded.DataSource = listMemberNotAdded;
                rptMemberAdded.DataBind();
                rptMemberNotAdded.DataBind();
                titleMemberAdded.InnerText = "Member Added : (" + listMemberAdded.Count + ")";
                titleMemberNotAdded.InnerText = "Member Not Added : (" + listMemberNotAdded.Count + ")";
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }

        }

        protected void exportButton_Click(object sender, EventArgs e)
        {
            try
            {
                string message = string.Empty;
                string nomEvenement = eventValue.Value.ToString().Trim();
                string anneeEdition = editionValue.Value.ToString().Trim();
                List<Personne> listMemberAddedInCsvFile = new List<Personne>();

                if (!string.IsNullOrEmpty(nomEvenement) && !string.IsNullOrEmpty(anneeEdition))
                {
                    // Get all the event's member
                    List<string> data = db.GetAllMemberByEventAndEdition(nomEvenement, anneeEdition, out message, out listMemberAddedInCsvFile);
                    rptMemberInCsvFile.DataSource = listMemberAddedInCsvFile;
                    rptMemberInCsvFile.DataBind();

                    if (data.Count > 0)
                    {

                        ShowMessage(message, "success");
                        // Todo create csv file
                        DateTime now = DateTime.Now;
                        string date = now.Day + "-" + now.Month + "-" + now.Year;
                        string rootPath = Server.MapPath("/App_Data");
                        string path = @"" + rootPath + "/memberExport_" + date + ".csv";
                        if (CreateCsvFile(data, path))
                        {
                            
                            WebClient req = new WebClient();
                            HttpResponse response = HttpContext.Current.Response;
                            response.Clear();
                            response.ClearContent();
                            response.ClearHeaders();
                            response.Buffer = true;
                            response.AddHeader("Content-Disposition", "attachment;filename=\"" + path + "\"");
                            byte[] dataByte = req.DownloadData(path);
                            response.BinaryWrite(dataByte);
                            response.Flush();
                            response.End();
                        }
                    }
                    else
                    {
                        ShowMessage(message, "error");
                    }
                }
                else
                {
                    ShowMessage("Une erreur est survenue. Le nom de l\'évènement ou l\'année de l\'édition est invalide.", "error");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
        }

        private bool CreateCsvFile(List<string> data, string path)
        {
            try
            {
                FileStream fs = null;
                if (!File.Exists(path))
                {
                    using (fs = File.Create(path))
                    {
                        StreamWriter writer = new StreamWriter(fs, Encoding.UTF8);
                        foreach (string ligne in data)
                        {
                            writer.WriteLine(ligne);
                        }
                        writer.Close();
                        writer.Dispose();
                    }
                }
                else
                {
                    File.Delete(path);
                    FileStream fs1 = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
                    using (StreamWriter sw = new StreamWriter(fs1, Encoding.UTF8))
                    {
                        foreach (string ligne in data)
                        {
                            sw.WriteLine(ligne);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {

            }
            return false;
        }
    }
}