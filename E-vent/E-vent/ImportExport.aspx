﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ImportExport.aspx.cs" Inherits="E_vent.ImportExport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div id="Export" class="col-lg-6 col-md-6 col-sm-6 col-xs-6" runat="server">
            <div class="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Exporter des membres</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <asp:Repeater ID="rptListEvent" runat="server">
                                <HeaderTemplate>
                                    <label for="selectEvent">Evènement</label>
                                    <select id="selectEvent" class="selectpicker form-control">
                                        <option selected>Aucun</option>
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <option><%# ((E_vent.Evenement)Container.DataItem).nom %></option>
                                </ItemTemplate>

                                <FooterTemplate>
                                    </select>
                        
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="selectEdition">Edition</label>
                            <select id="selectEdition" class="selectpicker form-control" onchange="onChangeEdition()" disabled>
                                <option selected>Aucune</option>
                            </select>

                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
                            <div id="divExportButton" class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="display: inline-block; margin-top: 15px;">
                                <asp:Button ID="exportButton" runat="server" OnClick="exportButton_Click" CssClass="btn btn-success btn-block" Text="Exporter" />
                            </div>
                        </div>
                        <asp:HiddenField ID="eventValue" runat="server" ClientIDMode="Static" Value="Aucun" />
                        <asp:HiddenField ID="editionValue" runat="server" ClientIDMode="Static" Value="Aucune" />

                        <div style="margin-top: 20px;">
                            <h4 id="titleMemberInCsvFile" runat="server">Member in Csv File : </h4>
                            <asp:Repeater ID="rptMemberInCsvFile" runat="server">
                                <HeaderTemplate>

                                    <div class="table-responsive" style="max-height: 500px;">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Prénom</th>
                                                    <th>Nom</th>
                                                    <th>Email</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <tr>
                                        <td><%# ((E_vent.Personne)Container.DataItem).prenom %></td>
                                        <td><%# ((E_vent.Personne)Container.DataItem).nom %></td>
                                        <td><%# ((E_vent.Personne)Container.DataItem).email %></td>
                                    </tr>
                                </ItemTemplate>

                                <FooterTemplate>
                                    </tbody>
                        </table>
                    </div>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div id="Import" class="col-lg-6 col-md-6 col-sm-6 col-xs-6" runat="server">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Importer des membres</h3>
                </div>
                <div class="panel-body">
                    <div class="input-group" style="margin-bottom: 15px;">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">Browse&hellip;
                                <asp:FileUpload ID="inputCsv" runat="server" CssClass="hidden" AllowMultiple="true" />
                                <%--<input id="inputCsv" runat="server" type="file" style="display: none;" multiple />--%>
                            </span>
                        </label>
                        <input type="text" class="form-control" readonly />
                    </div>
                    <asp:Button ID="btnImportCsv" runat="server" Text="Importer" CssClass="btn btn-success btn-block" OnClick="btnImportCsv_Click" />

                    <%--------------------------%>
                    <div style="margin-top: 20px;">
                        <h4 id="titleMemberAdded" runat="server">Member added : </h4>
                        <asp:Repeater ID="rptMemberAdded" runat="server">
                            <HeaderTemplate>

                                <div class="table-responsive" style="max-height: 500px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Prénom</th>
                                                <th>Nom</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                            </HeaderTemplate>

                            <ItemTemplate>
                                <tr style="color: green;">
                                    <td><%# ((E_vent.Personne)Container.DataItem).prenom %></td>
                                    <td><%# ((E_vent.Personne)Container.DataItem).nom %></td>
                                    <td><%# ((E_vent.Personne)Container.DataItem).email %></td>
                                </tr>
                            </ItemTemplate>

                            <FooterTemplate>
                                </tbody>
                        </table>
                    </div>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                    <%--------------------------%>

                    <div style="margin-top: 20px;">
                        <h4 id="titleMemberNotAdded" runat="server">Member Not Added : </h4>
                        <asp:Repeater ID="rptMemberNotAdded" runat="server">
                            <HeaderTemplate>

                                <div class="table-responsive" style="max-height: 500px;">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Prénom</th>
                                                <th>Nom</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                            </HeaderTemplate>

                            <ItemTemplate>
                                <tr style="color: red;">
                                    <td><%# ((E_vent.Personne)Container.DataItem).prenom %></td>
                                    <td><%# ((E_vent.Personne)Container.DataItem).nom %></td>
                                    <td><%# ((E_vent.Personne)Container.DataItem).email %></td>
                                </tr>
                            </ItemTemplate>

                            <FooterTemplate>
                                </tbody>
                        </table>
                </div>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>


                </div>
            </div>

        </div>

    </div>

    <script>

        $('#selectEvent').on('change', function () {
            var nomEvent = this.selectedOptions[0].innerText;
            if (nomEvent != "" && nomEvent != "Aucun") {
                PageMethods.GetJsonEdition(nomEvent, onSuccessEdition, onErrorEdition);
            }
            else {
                $('#selectEdition').children().remove();
                $('#selectEdition').append("<option value='0' selected>Aucune</option>");
                $('#selectEdition').prop('disabled', 'disabled');
                if (nomEvent == "Aucun") {
                    $("#divExportButton").show();
                }
                else {
                    $("#divExportButton").hide();
                }

            }
        });

        $('#selectEdition').on('change', function () {
            var idEdition = this.value;
            var nomEvenement = $("#selectEvent option:selected").text();
            var anneeEdition = this.selectedOptions[0].innerText;
            if (anneeEdition != "") {
                $("#editionValue").val(anneeEdition);
            }
            else {
                if (nomEvenement == "Aucun") {
                    $("#editionValue").val("Aucune");
                }
                else {
                    $("#editionValue").val("");
                }

            }
        });

        function onSuccessEdition(result) {
            try {
                var editions = JSON.parse(result);
                if (Object.keys(editions).length > 0) {
                    $("#selectEdition").children().remove();
                    for (var i in editions) {
                        $("#selectEdition").append("<option value='" + editions[i].id + "'>" + editions[i].annee + "</option>");
                    }
                    $("#selectEdition").prop('disabled', false);
                    var idEdition = $("#selectEdition option:selected").val();
                    var nomEvenement = $("#selectEvent option:selected").text();
                    $("#eventValue").val($("#selectEvent option:selected").text());
                    $("#editionValue").val($("#selectEdition option:selected").text());
                    $("#divExportButton").show();
                }
                else {
                    $("#selectEdition").append("<option value='0' selected>Aucune</option>");
                    $("#selectEdition").prop("disabled", "disabled");
                    $("#divExportButton").hide();
                }
                // si il y a au moins 1 resultat, on déverouille le select edition
            }
            catch (e) {
                console.log(e.message);
            }
        }
        function onErrorEdition(result) {
            console.log(result);
        }

        $(function () {

            // We can attach the `fileselect` event to all file inputs on the page
            $(document).on('change', ':file', function () {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            // We can watch for our custom `fileselect` event like this
            $(document).ready(function () {
                $(':file').on('fileselect', function (event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                        log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    } else {
                        if (log) alert(log);
                    }

                });
            });

        });

        function ShowMessage(message, messagetype) {
            console.log(message);
            console.log(messagetype);
            var cssclass;
            switch (messagetype) {
                case 'success':
                    cssclass = 'alert-success';
                    break;
                case 'error':
                    cssclass = 'alert-danger';
                    break;
                case 'warning':
                    cssclass = 'alert-warning';
                    break;
                default:
                    cssclass = 'alert-info';
            }
            $('#Body').before('<div id="alert_div" style="position: fixed; z-index: 999; left: 50%;width: 50%;margin-top: 20px;transform: translateX(-50%);" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>');
            $(".alert").fadeTo(5000, 0).slideUp(3000, function () {
                $(this).remove();
            });
        }
    </script>
</asp:Content>
