﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_vent
{
    public class Contact
    {
        public int id { get; set; }
        public string nom { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public string prenom { get; set; }
        public string sexe { get; set; }
        public DateTime dateNaissance { get; set; }
        public string dateNaissanceString { get; set; }
        public string poste { get; set; }
        public string adressePostale { get; set; }
        public int codePostal { get; set; }
        public string ville { get; set; }
        public string commentaire { get; set; }
        public bool isPayed { get; set; }
        public string taille { get; set; }
        public string regime { get; set; }
        public bool premiereFois { get; set; }
        public string photo { get; set; }
        public string inscription { get; set; }

        public Contact(string nom, string email, string prenom)
        {
            this.nom = nom;
            this.email = email;
            this.prenom = prenom;
        }

        public Contact(string nom, string email, string tel,
            string prenom, string sexe, DateTime datenaiss, string poste,
            string adressePostale, int codePostal, string ville,
            string commentaire, bool isPayed, string taille,
            string regime, bool premiereFois, string photo)
        {
            this.nom = nom;
            this.email = email;
            this.telephone = tel;
            this.prenom = prenom;
            this.sexe = sexe;
            this.dateNaissance = datenaiss;
            this.poste = poste;
            this.adressePostale = adressePostale;
            this.codePostal = codePostal;
            this.ville = ville;
            this.commentaire = commentaire;
            this.isPayed = isPayed;
            this.taille = taille;
            this.regime = regime;
            this.premiereFois = premiereFois;
            this.photo = photo;
        }

        public Contact(int id, string nom, string email, string tel,
            string prenom, string sexe, DateTime datenaiss, string poste,
            string adressePostale, int codePostal, string ville,
            string commentaire, bool isPayed, string taille,
            string regime, bool premiereFois, string photo)
        {
            this.id = id;
            this.nom = nom;
            this.email = email;
            this.telephone = tel;
            this.prenom = prenom;
            this.sexe = sexe;
            this.dateNaissance = datenaiss;
            this.poste = poste;
            this.adressePostale = adressePostale;
            this.codePostal = codePostal;
            this.ville = ville;
            this.commentaire = commentaire;
            this.isPayed = isPayed;
            this.taille = taille;
            this.regime = regime;
            this.premiereFois = premiereFois;
            this.photo = photo;
        }

        public Contact(string nom, string prenom,
            string dateNaissanceString, string adressePostale, string tel,
            string ville, int codePostal, string email,
            string poste, bool premiereFois,
            string commentaire, string inscription)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.dateNaissanceString = dateNaissanceString;
            this.adressePostale = adressePostale;
            this.telephone = tel;
            this.ville = ville;
            this.codePostal = codePostal;
            this.email = email;
            this.poste = poste;
            this.premiereFois = premiereFois;
            this.commentaire = commentaire;
            this.inscription = inscription;
        }
    }
}