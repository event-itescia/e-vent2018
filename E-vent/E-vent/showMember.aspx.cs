﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class showMember : PageBase
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            isAuthenticated();
            if (!IsPostBack)
            {
                List<Personne> allMembers = new List<Personne>();
                rptShowMember.DataSource = db.GetAllPersonne();
                rptShowMember.DataBind();
            }
        }
        [System.Web.Services.WebMethod]
        public static string getMembre(string id)
        {
            try
            {
                DBConnect db2 = new DBConnect();

                if (!string.IsNullOrEmpty(id))
                {
                    string result = db2.GetPersonneById(id);
                    if (!string.IsNullOrEmpty(result))
                    {
                        return result;
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return string.Empty;
        }
    }
}