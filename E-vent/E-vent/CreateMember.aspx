﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CreateMember.aspx.cs" Inherits="E_vent.CreateMember" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="max-width: 800px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Créer un membre</h3>
            </div>
            <div class="panel-body">
                <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <label>Prénom<span style="margin-left: 5px;">*</span></label>
                    <input id="prenom" runat="server" type="text" clientidmode="Static" class="form-control" autofocus required />
                </div>
                <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <label>Nom<span style="margin-left: 5px;">*</span></label>
                    <input id="nom" runat="server" type="text" clientidmode="Static" class="form-control" required />
                </div>
                <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <label>Sexe<span style="margin-left: 5px;">*</span></label>
                    <select id="sexe" runat="server" class="selectpicker form-control">
                        <option>Homme</option>
                        <option>Femme</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label>Jour de naissance<span style="margin-left: 5px;">*</span></label>
                    <select id="jourNaissance" class="selectpicker form-control" onchange="ChangeJourNaissance()">
                        <script>
                            for (i = 1; i < 31; i++) {
                                $("#jourNaissance").append("<option>" + i + "</option>");
                            }
                        </script>
                    </select>
                    <asp:HiddenField ID="jourNaiss" runat="server" ClientIDMode="Static" />
                    <script>$("#jourNaiss").val($("#jourNaissance option:selected").text());</script>
                </div>

                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label>Mois de naissance<span style="margin-left: 5px;">*</span></label>
                    <select id="moisNaissance" runat="server" class="selectpicker form-control">
                        <option value="janvier">Janvier</option>
                        <option value="février">Février</option>
                        <option value="mars">Mars</option>
                        <option value="avril">Avril</option>
                        <option value="mai">Mai</option>
                        <option value="juin">Juin</option>
                        <option value="juillet">Juillet</option>
                        <option value="août">Août</option>
                        <option value="septembre">Septembre</option>
                        <option value="octobre">Octobre</option>
                        <option value="novembre">Novembre</option>
                        <option value="décembre">Décembre</option>
                    </select>
                </div>

                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label>Année de naissance<span style="margin-left: 5px;">*</span></label>
                    <select id="anneeNaissance" class="selectpicker form-control" onchange="ChangeAnneeNaissance()">
                        <script>
                            var today = new Date();
                            var limiteAge = 10;
                            for (i = today.getFullYear() - limiteAge; i > 1950; i--) {
                                $("#anneeNaissance").append("<option>" + i + "</option>");
                            }
                        </script>
                    </select>
                    <asp:HiddenField ID="anneeNaiss" runat="server" ClientIDMode="Static" />
                    <script>$("#anneeNaiss").val($("#anneeNaissance option:selected").text());</script>
                </div>

                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label for="mail">Mail<span style="margin-left: 5px;">*</span></label>
                    <input id="mail" runat="server" type="email" clientidmode="Static" class="form-control" required />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="tel">Téléphone</label>
                    <input id="tel" runat="server" type="tel" clientidmode="Static" class="form-control" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="commentaire">Commentaire</label>
                    <input id="commentaire" runat="server" type="tel" clientidmode="Static" class="form-control" />
                </div>
                <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label for="adresse">Adresse</label>
                    <input id="adresse" runat="server" type="text" clientidmode="Static" class="form-control" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="ville">Ville</label>
                    <input id="ville" runat="server" type="text" clientidmode="Static" class="form-control" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="cp">Code Postal</label>
                    <input id="cp" runat="server" type="text" clientidmode="Static" class="form-control" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="isPayed">A-t-il déjà réglé ?<span style="margin-left: 5px;">*</span></label>
                    <select id="isPayed" runat="server" class="selectpicker form-control">
                        <option selected>Non</option>
                        <option>Oui</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="taille">Taille</label>
                    <select id="taille" runat="server" class="selectpicker form-control">
                        <option>XS</option>
                        <option>S</option>
                        <option selected>M</option>
                        <option>L</option>
                        <option>XL</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="regime">Régime alimentaire</label>
                    <select id="regime" runat="server" class="selectpicker form-control">
                        <option>Végétalien</option>
                        <option>Végan</option>
                        <option>Pescetarien</option>
                        <option>Flexitarien</option>
                        <option selected>Mange de tout</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="firstParticipation">1ère participation ?</label>
                    <select id="firstParticipation" runat="server" class="selectpicker form-control">
                        <option>Non</option>
                        <option selected>Oui</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="association">Association</label>
                    <select id="association" runat="server" clientidmode="Static" class="selectpicker form-control">
                        <option value="0">Aucune</option>
                    </select>
                    <asp:HiddenField ID="associationValue" runat="server" ClientIDMode="Static" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="evenement">Evènement<span style="margin-left: 5px;">*</span></label>
                    <select id="evenement" runat="server" clientidmode="Static" class="selectpicker form-control">
                        <option value="0">Aucun</option>
                    </select>
                    <asp:HiddenField ID="eventValue" runat="server" ClientIDMode="Static" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="edition">Edition<span style="margin-left: 5px;">*</span></label>
                    <select id="edition" runat="server" clientidmode="Static" class="selectpicker form-control" disabled>
                        <option value="0">Aucune</option>
                    </select>
                    <asp:HiddenField ID="editionValue" runat="server" ClientIDMode="Static" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="groupe">Groupe</label>
                    <select id="groupe" runat="server" clientidmode="Static" class="selectpicker form-control" disabled>
                        <option value="0">Aucun</option>
                    </select>
                    <asp:HiddenField ID="groupeValue" runat="server" ClientIDMode="Static" />
                </div>
                <div class="form-group col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <label for="poste">Poste</label>
                    <select id="poste" runat="server" class="selectpicker form-control">
                        <option selected>Développeur</option>
                        <option>Graphiste</option>
                        <option>Designer</option>
                        <option>Game designer</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3" style="padding: 0;">
                        <label for="photo">Photo</label>
                        <input id="photo" runat="server" type="file" clientidmode="Static" class="" />
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12" style="text-align: center; margin-top: 30px;">
                    <asp:Button ID="btnCreateMember" runat="server" OnClick="btnCreateMember_Click" CssClass="btn btn-success btn-lg" Text="Ajouter un membre" />
                </div>

            </div>
        </div>

    </div>

    <script>

        $("#association").on('change', function () {
            $("#associationValue").val($("#association option:selected").text());
        });

        $('#evenement').on('change', function () {
            var idEvent = this.value;
            var nomEvent = this.selectedOptions[0].innerText;

            if (nomEvent != "Aucun") {
                PageMethods.GetJsonEdition(nomEvent, onSuccessEdition, onErrorEdition);
            }
            else {
                $('#edition').prop('disabled', 'disabled');
                $('#edition').children().remove();
                $('#edition').append("<option value='0' selected>Aucune</option>");
                $('#groupe').prop('disabled', 'disabled');
                $('#groupe').children().remove();
                $('#groupe').append("<option value='0' selected>Aucun</option>");
            }
            $("#eventValue").val($("#evenement option:selected").text());
            $("#editionValue").val($("#edition option:selected").text());
            $("#groupeValue").val($("#groupe option:selected").text());
        });

        $('#edition').on('change', function () {
            var idEdition = this.value;
            var nomEvenement = $("#evenement option:selected").text();
            var anneeEdition = this.selectedOptions[0].innerText;

            if (anneeEdition != "Aucune") {
                PageMethods.GetJsonGroupe(idEdition, nomEvenement, onSuccessGroupe, onErrorGroupe);
            }
            else {
                $('#groupe').prop('disabled', 'disabled');
                $('#groupe').children().remove();
                $('#groupe').append("<option value='0' selected>Aucun</option>");
            }
            $("#eventValue").val($("#evenement option:selected").text());
            $("#editionValue").val($("#edition option:selected").text());
            $("#groupeValue").val($("#groupe option:selected").text());
        });
        $('#groupe').on('change', function () {
            $("#eventValue").val($("#evenement option:selected").text());
            $("#editionValue").val($("#edition option:selected").text());
            $("#groupeValue").val($("#groupe option:selected").text());
        });

        function onSuccessEdition(result) {
            try {
                var editions = JSON.parse(result);
                if (Object.keys(editions).length > 0) {
                    $('#edition').children().remove();
                    for (var i in editions) {
                        $('#edition').append("<option value='" + editions[i].id + "'>" + editions[i].annee + "</option>");
                    }
                    $('#edition').prop('disabled', false);
                    var idEdition = $("#edition option:selected").val();
                    var nomEvenement = $("#evenement option:selected").text();
                    $("#eventValue").val($("#evenement option:selected").text());
                    $("#editionValue").val($("#edition option:selected").text());
                    $("#groupeValue").val($("#groupe option:selected").text());
                    PageMethods.GetJsonGroupe(idEdition, nomEvenement, onSuccessGroupe, onErrorGroupe);
                }
                else {
                    $('#edition').append("<option value='0' selected>Aucune</option>");
                    $('#edition').prop('disabled', 'disabled');
                }
                // si il y a au moins 1 resultat, on déverouille le select edition
            }
            catch (e) {
                console.log(e.message);
            }
        }
        function onErrorEdition(result) {
            console.log(result);
        }

        function onSuccessGroupe(result) {
            try {
                var groupes = JSON.parse(result);
                $('#groupe').children().remove();
                $('#groupe').append("<option value='0' selected>Aucun</option>");
                if (Object.keys(groupes).length > 0) {
                    for (var i in groupes) {
                        $('#groupe').append("<option value='" + groupes[i].id + "'>" + groupes[i].nomGroupe + "</option>");
                    }
                    $('#groupe').prop('disabled', false);
                    $("#eventValue").val($("#evenement option:selected").text());
                    $("#editionValue").val($("#edition option:selected").text());
                    $("#groupeValue").val($("#groupe option:selected").text());
                }
                else {
                    $('#groupe').prop('disabled', 'disabled');
                }
            }
            catch (e) {
                console.log(e.message);
            }
        }

        function onErrorGroupe(result) {
            console.log(result);
        }


        $(document).ready(function () {
            PageMethods.GetJsonAssociation(onSuccessAssociation, onErrorAssociation);
            PageMethods.GetJsonEvenement(onSuccessEvenement, onErrorEvenement);

            function onSuccessAssociation(result) {
                try {
                    var associations = JSON.parse(result);
                    for (var i in associations) {
                        $('#association').append("<option value='" + associations[i].id + "'>" + associations[i].nom + "</option>");
                    }
                    $("#associationValue").val($("#association option:selected").text());
                }
                catch (e) {
                    console.log(e.message);
                }

            }
            function onErrorAssociation(result) {
                console.log(result);
            }

            function onSuccessEvenement(result) {
                try {
                    var evenements = JSON.parse(result);
                    for (var i in evenements) {
                        $('#evenement').append("<option value='" + evenements[i].id + "'>" + evenements[i].nom + "</option>");
                    }
                    $("#eventValue").val($("#evenement option:selected").text());
                    $("#editionValue").val($("#edition option:selected").text());
                    $("#groupeValue").val($("#groupe option:selected").text());
                }
                catch (e) {
                    console.log(e.message);
                }
            }
            function onErrorEvenement(result) {
                console.log(result);
            }




        });
        function ChangeJourNaissance() {
            $("#jourNaiss").val($("#jourNaissance option:selected").text());
        }

        function ChangeAnneeNaissance() {
            $("#anneeNaiss").val($("#anneeNaissance option:selected").text());
        }

        function ShowMessage(message, messagetype) {
            console.log(message);
            console.log(messagetype);
            var cssclass;
            switch (messagetype) {
                case 'success':
                    cssclass = 'alert-success';
                    break;
                case 'error':
                    cssclass = 'alert-danger';
                    break;
                case 'warning':
                    cssclass = 'alert-warning';
                    break;
                default:
                    cssclass = 'alert-info';
            }
            $('#Body').before('<div id="alert_div" style="position: fixed; z-index: 999; left: 50%;width: 50%;margin-top: 20px;transform: translateX(-50%);" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>');
            $(".alert").fadeTo(5000, 0).slideUp(1500, function () {
                $(this).remove();
            });
        }
    </script>
</asp:Content>
