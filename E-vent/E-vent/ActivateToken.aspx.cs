﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class ActivateToken : PageBase
    {
        DBConnect db = new DBConnect();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["user"] != null && !string.IsNullOrEmpty(Request.QueryString["user"]) &&
                    Request.QueryString["token"] != null && !string.IsNullOrEmpty(Request.QueryString["token"]))
            {
                string emailUser = Request.QueryString["user"];
                string token = Request.QueryString["token"];
                if (db.CheckToken(emailUser, token))
                {
                    AuthenticationPanel.Visible = true;
                }
                else
                {
                    HttpContext.Current.Response.Redirect("~/Login.aspx");
                }
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/Login.aspx");
            }
        }

        protected void ChangePassword_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["user"] != null && !string.IsNullOrEmpty(Request.QueryString["user"]) && inputPassword.Value == inputPassword2.Value)
            {
                string login = Request.QueryString["user"].ToString().Trim();
                string password512 = GetSha512(Encrypt(inputPassword.Value.Trim()));
                if (db.ReinitializePassword(login, password512))
                {
                    AuthenticationPanel.Visible = false;
                    ImageSuccess.Visible = true;
                    ImageDenied.Visible = false;
                    Thread.Sleep(2000);
                    HttpContext.Current.Response.Redirect("~/Login.aspx");
                }
                else
                {
                    
                }
            }
        }
    }
}