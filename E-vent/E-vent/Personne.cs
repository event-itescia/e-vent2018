﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_vent
{
    public class Personne : Contact
    {
        public Personne(string name, string surname, string email) : base(name, surname, email)
        {
            this.nom = name;
            this.prenom = surname;
            this.email = email;
        }

        public Personne(string name, string surname, string dateNaissString, string adresse, string tel, string ville, int cp, string email, string poste, bool premiereFois, string commentaire, string inscription) : base(name, surname, dateNaissString, adresse, tel, ville, cp, email, poste, premiereFois, commentaire, inscription)
        {
            this.nom = string.IsNullOrEmpty(nom) ? "NULL" : nom;
            this.prenom = string.IsNullOrEmpty(prenom) ? "NULL" : prenom;
            this.dateNaissanceString = string.IsNullOrEmpty(dateNaissString) ? "NULL" : dateNaissString;
            this.adressePostale = string.IsNullOrEmpty(adressePostale) ? "NULL" : adressePostale;
            this.telephone = string.IsNullOrEmpty(tel) ? "NULL" : tel;
            this.ville = string.IsNullOrEmpty(ville) ? "NULL" : ville;
            this.codePostal = codePostal == 0 ? 0 : codePostal;
            this.email = string.IsNullOrEmpty(email) ? "NULL" : email;
            this.poste = string.IsNullOrEmpty(poste) ? "NULL" : poste;
            this.premiereFois = premiereFois;
            this.commentaire = string.IsNullOrEmpty(commentaire) ? "NULL" : commentaire;
            this.inscription = inscription;
        }

        public Personne(string nom, string email, string tel, string prenom, string sexe, DateTime datenaiss, string poste, string adressePostale, int codePostal, string ville, string commentaire, bool isPayed, string taille, string regime, bool premiereFois, string photo) : base(nom, email, tel, prenom, sexe, datenaiss, poste, adressePostale, codePostal, ville, commentaire, isPayed, taille, regime, premiereFois, photo)
        {
            this.nom = string.IsNullOrEmpty(nom) ? "NULL" : nom;
            this.email = string.IsNullOrEmpty(email) ? "NULL" : email;
            this.telephone = string.IsNullOrEmpty(tel) ? "NULL" : tel;
            this.prenom = string.IsNullOrEmpty(prenom) ? "NULL" : prenom;
            this.sexe = sexe;
            this.dateNaissance = datenaiss;
            this.poste = string.IsNullOrEmpty(poste) ? "NULL" : poste;
            this.adressePostale = string.IsNullOrEmpty(adressePostale) ? "NULL" : adressePostale;
            this.codePostal = codePostal == 0 ? 0 : codePostal;
            this.ville = string.IsNullOrEmpty(ville) ? "NULL" : ville;
            this.commentaire = string.IsNullOrEmpty(commentaire) ? "NULL" : commentaire;
            this.isPayed = isPayed;
            this.taille = string.IsNullOrEmpty(taille) ? "NULL" : taille;
            this.regime = string.IsNullOrEmpty(regime) ? "NULL" : regime;
            this.premiereFois = premiereFois;
            this.photo = string.IsNullOrEmpty(photo) ? "NULL" : photo;
        }

        

        public Personne(int id, string nom, string email, string tel, string prenom, string sexe, DateTime datenaiss, string poste, string adressePostale, int codePostal, string ville, string commentaire, bool isPayed, string taille, string regime, bool premiereFois, string photo) : base(id, nom, email, tel, prenom, sexe, datenaiss, poste, adressePostale, codePostal, ville, commentaire, isPayed, taille, regime, premiereFois, photo)
        {
            this.id = id;
            this.nom = string.IsNullOrEmpty(nom) ? "NULL" : nom;
            this.email = string.IsNullOrEmpty(email) ? "NULL" : email;
            this.telephone = string.IsNullOrEmpty(tel) ? "NULL" : tel;
            this.prenom = string.IsNullOrEmpty(prenom) ? "NULL" : prenom;
            this.sexe = sexe;
            this.dateNaissance = datenaiss;
            this.poste = string.IsNullOrEmpty(poste) ? "NULL" : poste;
            this.adressePostale = string.IsNullOrEmpty(adressePostale) ? "NULL" : adressePostale;
            this.codePostal = codePostal == 0 ? 0 : codePostal;
            this.ville = string.IsNullOrEmpty(ville) ? "NULL" : ville;
            this.commentaire = string.IsNullOrEmpty(commentaire) ? "NULL" : commentaire;
            this.isPayed = isPayed;
            this.taille = string.IsNullOrEmpty(taille) ? "NULL" : taille;
            this.regime = string.IsNullOrEmpty(regime) ? "NULL" : regime;
            this.premiereFois = premiereFois;
            this.photo = string.IsNullOrEmpty(photo) ? "NULL" : photo;
        }
    }
}