﻿<%@ Page Title="Création de groupes" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CreateGroup.aspx.cs" Inherits="E_vent.CreateGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div style="max-width: 800px;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Créer un groupe</h3>
            </div>
            <div class="panel-body">
                <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <label>Nom du groupe<span style="margin-left: 5px;">*</span></label>
                    <input id="nomGroupe" runat="server" type="text" clientidmode="Static" class="form-control" autofocus required />
                </div>
            </div>
        </div>

    </div>

    <script>

        $("#association").on('change', function () {
            $("#associationValue").val($("#association option:selected").text());
        });

        $('#evenement').on('change', function () {
            var idEvent = this.value;
            var nomEvent = this.selectedOptions[0].innerText;

            if (nomEvent != "Aucun") {
                PageMethods.GetJsonEdition(nomEvent, onSuccessEdition, onErrorEdition);
            }
            else {
                $('#edition').prop('disabled', 'disabled');
                $('#edition').children().remove();
                $('#edition').append("<option value='0' selected>Aucune</option>");
                $('#groupe').prop('disabled', 'disabled');
                $('#groupe').children().remove();
                $('#groupe').append("<option value='0' selected>Aucun</option>");
            }
            $("#eventValue").val($("#evenement option:selected").text());
            $("#editionValue").val($("#edition option:selected").text());
            $("#groupeValue").val($("#groupe option:selected").text());
        });

        $('#edition').on('change', function () {
            var idEdition = this.value;
            var nomEvenement = $("#evenement option:selected").text();
            var anneeEdition = this.selectedOptions[0].innerText;

            if (anneeEdition != "Aucune") {
                PageMethods.GetJsonGroupe(idEdition, nomEvenement, onSuccessGroupe, onErrorGroupe);
            }
            else {
                $('#groupe').prop('disabled', 'disabled');
                $('#groupe').children().remove();
                $('#groupe').append("<option value='0' selected>Aucun</option>");
            }
            $("#eventValue").val($("#evenement option:selected").text());
            $("#editionValue").val($("#edition option:selected").text());
            $("#groupeValue").val($("#groupe option:selected").text());
        });
        $('#groupe').on('change', function () {
            $("#eventValue").val($("#evenement option:selected").text());
            $("#editionValue").val($("#edition option:selected").text());
            $("#groupeValue").val($("#groupe option:selected").text());
        });

        function onSuccessEdition(result) {
            try {
                var editions = JSON.parse(result);
                if (Object.keys(editions).length > 0) {
                    $('#edition').children().remove();
                    for (var i in editions) {
                        $('#edition').append("<option value='" + editions[i].id + "'>" + editions[i].annee + "</option>");
                    }
                    $('#edition').prop('disabled', false);
                    var idEdition = $("#edition option:selected").val();
                    var nomEvenement = $("#evenement option:selected").text();
                    $("#eventValue").val($("#evenement option:selected").text());
                    $("#editionValue").val($("#edition option:selected").text());
                    $("#groupeValue").val($("#groupe option:selected").text());
                    PageMethods.GetJsonGroupe(idEdition, nomEvenement, onSuccessGroupe, onErrorGroupe);
                }
                else {
                    $('#edition').append("<option value='0' selected>Aucune</option>");
                    $('#edition').prop('disabled', 'disabled');
                }
                // si il y a au moins 1 resultat, on déverouille le select edition
            }
            catch (e) {
                console.log(e.message);
            }
        }
        function onErrorEdition(result) {
            console.log(result);
        }

        function onSuccessGroupe(result) {
            try {
                var groupes = JSON.parse(result);
                $('#groupe').children().remove();
                $('#groupe').append("<option value='0' selected>Aucun</option>");
                if (Object.keys(groupes).length > 0) {
                    for (var i in groupes) {
                        $('#groupe').append("<option value='" + groupes[i].id + "'>" + groupes[i].nomGroupe + "</option>");
                    }
                    $('#groupe').prop('disabled', false);
                    $("#eventValue").val($("#evenement option:selected").text());
                    $("#editionValue").val($("#edition option:selected").text());
                    $("#groupeValue").val($("#groupe option:selected").text());
                }
                else {
                    $('#groupe').prop('disabled', 'disabled');
                }
            }
            catch (e) {
                console.log(e.message);
            }
        }

        function onErrorGroupe(result) {
            console.log(result);
        }


        $(document).ready(function () {
            PageMethods.GetJsonAssociation(onSuccessAssociation, onErrorAssociation);
            PageMethods.GetJsonEvenement(onSuccessEvenement, onErrorEvenement);

            function onSuccessAssociation(result) {
                try {
                    var associations = JSON.parse(result);
                    for (var i in associations) {
                        $('#association').append("<option value='" + associations[i].id + "'>" + associations[i].nom + "</option>");
                    }
                    $("#associationValue").val($("#association option:selected").text());
                }
                catch (e) {
                    console.log(e.message);
                }

            }
            function onErrorAssociation(result) {
                console.log(result);
            }

            function onSuccessEvenement(result) {
                try {
                    var evenements = JSON.parse(result);
                    for (var i in evenements) {
                        $('#evenement').append("<option value='" + evenements[i].id + "'>" + evenements[i].nom + "</option>");
                    }
                    $("#eventValue").val($("#evenement option:selected").text());
                    $("#editionValue").val($("#edition option:selected").text());
                    $("#groupeValue").val($("#groupe option:selected").text());
                }
                catch (e) {
                    console.log(e.message);
                }
            }
            function onErrorEvenement(result) {
                console.log(result);
            }




        });
        function ChangeJourNaissance() {
            $("#jourNaiss").val($("#jourNaissance option:selected").text());
        }

        function ChangeAnneeNaissance() {
            $("#anneeNaiss").val($("#anneeNaissance option:selected").text());
        }

        function ShowMessage(message, messagetype) {
            console.log(message);
            console.log(messagetype);
            var cssclass;
            switch (messagetype) {
                case 'success':
                    cssclass = 'alert-success';
                    break;
                case 'error':
                    cssclass = 'alert-danger';
                    break;
                case 'warning':
                    cssclass = 'alert-warning';
                    break;
                default:
                    cssclass = 'alert-info';
            }
            $('#Body').before('<div id="alert_div" style="position: fixed; z-index: 999; left: 50%;width: 50%;margin-top: 20px;transform: translateX(-50%);" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>');
            $(".alert").fadeTo(5000, 0).slideUp(1500, function () {
                $(this).remove();
            });
        }
    </script>
</asp:Content>