﻿<%@ Page Title="Affichage des membres" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="showMember.aspx.cs" Inherits="E_vent.showMember" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
                        </asp:ScriptManager>--%>
    <div class="">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Les membres</h3>
                <div class="" style="margin-top: 10px;">
                    <input type="text" placeholder="Search.." name="search" onfocus="this.value=''" id="inputSearchEvent" runat="server" />
                    <%-- %><asp:Button ID="btnSearchEvent" runat="server" OnClick="btnSearchEvent_Click" Text="Rechercher"></asp:Button>--%>
                </div>
            </div>
            <div class="panel-body">
                <asp:Repeater ID="rptShowMember" runat="server">
                    <HeaderTemplate>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th style="text-align: center;">Date de naissance</th>
                                        <th style="text-align: center;">Tel</th>
                                        <th style="text-align: center;">Sexe</th>
                                        <th style="text-align: center;">Afficher</th>
                                    </tr>
                                </thead>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# ((E_vent.Personne)Container.DataItem).id %></td>
                            <td><%# ((E_vent.Personne)Container.DataItem).nom %></td>
                            <td><%# ((E_vent.Personne)Container.DataItem).prenom %></td>
                            <td style="text-align: center;"><%#((E_vent.Personne)Container.DataItem).dateNaissance %></td>
                            <td style="text-align: center;"><%#((E_vent.Personne)Container.DataItem).telephone %></td>
                            <td style="text-align: center;"><%#((E_vent.Personne)Container.DataItem).sexe %></td>
                            <td>
                                <button name="btnShow" type="button" class="btn btn-block btn-success" data-toggle="modal" data-id="<%# ((E_vent.Personne)Container.DataItem).id %>" data-target="#myModal">Afficher</button>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>


                <asp:HiddenField ID="idPersonne" runat="server" ClientIDMode="static" />

                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <%  %>
                    <div class="modal-dialog modal-lg">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title" id="titleModal">Détail des membres</h4>

                            </div>
                            <div class="modal-body">

                                <%--NOM--%>
                                <div class="form-group">
                                    <label style="font-weight: 100;" for="eventName"><b>Informations sur le membre :</b> </label>

                                    <div class="form-group">
                                        <label>Nom :</label>
                                        <span class="form-control" id="nomMembre"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Prenom :</label>
                                        <span class="form-control" id="prenomMembre"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>E-mail :</label>
                                        <span class="form-control" id="emailMembre"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Sexe :</label>
                                        <span class="form-control" id="sexeMembre"></span>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <script>

                function EnterKey(e) {
                    //if (e.keyCode == 13) {
                    //    var div = e.currentTarget;
                    //    var idDiv = div.id;
                    //    var newContent = $("#" + idDiv).val();
                    //    var oldContent = document.getElementById("old" + idDiv).value;
                    //    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };

                    //    if (newContent != "") {
                    //        if (idDiv == "eventDebut" || idDiv == "eventFin") {
                    //            $("#" + idDiv).replaceWith('<label class="editable" id="' + idDiv + '" onkeypress="return EnterKey(event)">' + new Date(newContent).toLocaleDateString('fr-FR', options) + '</label>');
                    //            document.getElementById("old" + idDiv).value = newContent;
                    //            $(".bootstrap-datetimepicker-widget.dropdown-menu.bottom").remove();
                    //        }
                    //        else {
                    //            $("#" + idDiv).replaceWith('<label class="editable" id="' + idDiv + '" onkeypress="return EnterKey(event)">' + newContent + '</label>');
                    //            document.getElementById("old" + idDiv).value = newContent;
                    //        }

                    //    }
                    //    else {
                    //        $("#" + idDiv).replaceWith('<label class="editable" id="' + idDiv + '" onkeypress="return EnterKey(event)">' + oldContent + '</label>');
                    //    }
                    //}
                }

                $(document).on("click", "button[name='btnShow']", function () {
                    var idPersonne = $(this).data('id');
                    $("#idPersonne").val(idPersonne);

                    PageMethods.getMembre(idPersonne, onSuccess, onError);

                    function onSuccess(result) {
                        var personne = JSON.parse(result);
                        var nom = personne.nom;
                        var prenom = personne.prenom;
                        var email = personne.email;
                        var sexe = personne.sexe;

                        $("#nomMembre").text(nom);
                        $("#prenomMembre").text(prenom);
                        $("#emailMembre").text(email);
                        $("#sexeMembre").text(sexe);
                    }

                    function onError(result) {
                        alert('Cannot process your request at the moment, please try later.');
                    }
                });

                function ShowMessage(message, messagetype) {
                    console.log(message);
                    console.log(messagetype);
                    var cssclass;
                    switch (messagetype) {
                        case 'success':
                            cssclass = 'alert-success';
                            break;
                        case 'error':
                            cssclass = 'alert-danger';
                            break;
                        case 'warning':
                            cssclass = 'alert-warning';
                            break;
                        default:
                            cssclass = 'alert-info';
                    }
                    $('#Body').before('<div id="alert_div" style="position: fixed; z-index: 999; left: 50%;width: 50%;margin-top: 20px;transform: translateX(-50%);" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>');
                    $(".alert").fadeTo(5000, 0).slideUp(1500, function () {
                        $(this).remove();
                    });
                }

            </script>
</asp:Content>
