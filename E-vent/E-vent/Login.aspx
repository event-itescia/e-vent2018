﻿<%@ Page Title="Login" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="E_vent.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>E-Vent</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="stylesheet" href="Content/Css/bootstrap.min.css" />
    <%--<link rel="stylesheet" href="Content/Css/font-awesome.min.css" />--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script src="Content/Javascript/jquery.min.js"></script>
    <script src="Content/Javascript/bootstrap.min.js"></script>


    <style>
        html {
            background-image: url("Content/Images/fondGGJ.jpg");
            width: 100%;
            height: 100%;
        }

        #btnBack {
            width: 50px;
        }

        .fade {
            opacity: 0;
            -webkit-transition: opacity 0.15s linear;
            -moz-transition: opacity 0.15s linear;
            -o-transition: opacity 0.15s linear;
            transition: opacity 0.15s linear;
        }

            .fade.in {
                opacity: 1;
            }

        .messagealert {
            width: 100%;
            position: fixed;
            top: 0px;
            z-index: 100000;
            padding: 0;
            font-size: 15px;
        }

        .hidepanel {
            display: none;
        }
    </style>
</head>
<body style="color: white !important;">

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>

                <div id="bodyLogin" runat="server" style="right: 0;">

                    <div id="AuthenticationPanel" class="row" runat="server" style="width: 100%; position: fixed; top: 24%; height: auto !important;">
                        <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4"></div>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4" style="text-align: center; background-color: #313A4D; opacity: 0.7; padding: 10px 30px; padding-bottom: 10px; border-radius: 3px;">
                            <div class="form-group" style="text-align: center;">
                                <h2 class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin: 0;">Bienvenue sur E-vent</h2>
                                <h4 class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Veuillez saisir vos identifiants</h4>
                            </div>
                            <div class="form-group">
                                <label for="inputLogin" class="sr-only">Login :</label>
                                <input runat="server" type="text" id="inputLogin" class="form-control" placeholder="Adresse e-mail" required="" autofocus="" />
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="sr-only">Mot de passe :</label>
                                <input runat="server" type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" required="" />
                            </div>
                            <div class="form-group">
                                <label>
                                    <asp:CheckBox ID="chkRemember" runat="server" />
                                    Se souvenir de moi</label>
                            </div>
                            <div class="form-group">
                                <asp:Button ID="btnConnect" runat="server" CssClass="btn btn-success" Text="Connexion" OnClick="btnConnect_Click" />
                            </div>
                            <div class="form-group">
                                <asp:LinkButton ID="btnForgotPassword" OnClick="btnForgotPassword_Click" runat="server">Mot de passe oublié ? </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div id="panelForgotPassword" runat="server" visible="false" style="width: 100%; position: fixed; top: 24%; height: auto !important;">
                        <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4"></div>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4" style="text-align: center; background-color: #313A4D; opacity: 0.7; padding: 10px 30px; padding-bottom: 10px; border-radius: 3px;">
                            <div style="text-align: left;">
                                <asp:ImageButton ID="btnBack" runat="server" OnClick="btnBack_Click" ImageUrl="~/Content/Images/left-arrow.png" />
                            </div>

                            <div class="form-group" style="text-align: center;">
                                <h4 class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Récupération de mot de passe</h4>
                            </div>
                            <div class="form-group">
                                <label for="inputLoginFP" class="sr-only">Adresse e-mail ou login :</label>
                                <input runat="server" type="text" id="inputLoginFP" class="form-control" placeholder="Adresse e-mail ou Login" autofocus="" />
                            </div>
                            <div class="form-group">
                                <asp:Button ID="SendEmailForgotPassword" runat="server" CssClass="btn btn-success" Text="Confirmer" OnClick="SendEmailForgotPassword_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>

    <script>
        $('.input').keypress(function (e) {
            if (e.which == 13) {
                $('form#form1').submit();
                return false;
            }
        });

        function AnimatePanel() {
            $("#emailPanel").animate({ left: '-100%' });
        }

        $(document).ready(function () {
            window.setTimeout(function () {
                $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                    $(this).remove();
                });
            }, 3000);
        });

        function HideAlert() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }

        function ShowMessage(message, messagetype) {
            console.log(message);
            console.log(messagetype);
            var cssclass;
            switch (messagetype) {
                case 'success':
                    cssclass = 'alert-success';
                    break;
                case 'error':
                    cssclass = 'alert-danger';
                    break;
                case 'warning':
                    cssclass = 'alert-warning';
                    break;
                default:
                    cssclass = 'alert-info';
            }
            $('body').append('<div id="alert_div" style="position: fixed; left: 50%;width: 50%;margin-top: 20px;transform: translateX(-50%);" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>');
            $(".alert").fadeTo(5000, 0).slideUp(1500, function () {
                $(this).remove();
            });
        }
    </script>
</body>
</html>
