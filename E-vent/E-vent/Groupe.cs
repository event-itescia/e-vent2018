﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_vent
{
    public class Groupe
    {
        public int id { get; set; }
        public string nomGroupe { get; set; }
        public int idEvenement { get; set; }

        public Groupe()
        {

        }

        public Groupe(int id, string nomGroupe, int idEvenement)
        {
            this.id = id;
            this.nomGroupe = nomGroupe;
            this.idEvenement = idEvenement;
        }
    }
}