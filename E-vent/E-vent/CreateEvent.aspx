﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CreateEvent.aspx.cs" Inherits="E_vent.CreateEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="AlertPanel" runat="server">
        <Triggers>
            
        </Triggers>
        <ContentTemplate>

        </ContentTemplate>
    </asp:UpdatePanel>



    <asp:UpdatePanel runat="server" ID="panelCreateEvent">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="bodyContainer">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Créer un évènement</h3>
                    </div>
                    <div class="panel-body">

                        <%--NOM--%>
                        <div class="form-group">
                            <label for="nomEvent">Nom :</label>
                            <input runat="server" type="text" class="form-control" id="nomEvent" placeholder="Nom de l'évènement" />
                        </div>

                        <%--DATE--%>
                        <div id="formDate" class="form-group">
                            <label for="dateEvent">Date :</label>
                            <input id="dateRangePicker" class="form-control" type="text" name="datetimes" />
                        </div>
                        <script>

                            $(function () {
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!
                                var yyyy = today.getFullYear();
                                $('#dateRangePicker').daterangepicker({
                                    timePicker: true,
                                    "minYear": 2018,
                                    "maxYear": 2100,
                                    "locale": {
                                        "format": "DD/MM/YYYY HH:mm",
                                        "separator": " --> ",
                                        "applyLabel": "Valider",
                                        "cancelLabel": "Annuler",
                                        "fromLabel": "De",
                                        "toLabel": "à",
                                        "customRangeLabel": "Custom",
                                        "weekLabel": "W",
                                        "daysOfWeek": [
                                            "Lun",
                                            "Mar",
                                            "Mer",
                                            "Jeu",
                                            "Ven",
                                            "Sam",
                                            "Dim"
                                        ],
                                        "monthNames": [
                                            "Janvier",
                                            "Février",
                                            "Mars",
                                            "Avril",
                                            "Mai",
                                            "Juin",
                                            "Juillet",
                                            "Août",
                                            "Septembre",
                                            "Octobre",
                                            "Novembre",
                                            "Decembre"
                                        ],
                                        "firstDay": 1
                                    },
                                    "parentEl": "formDate",
                                    "startDate": dd + "-" + mm + "-" + yyyy + " " + 18 + ":" + 00,
                                    "endDate": dd + 3 + "-" + mm + "-" + yyyy + " " + 20 + ":" + 00,
                                    "cancelClass": "btn-danger",
                                    timePicker24Hour: true
                                }, function (start, end, label) {
                                    document.getElementById("startEvent").value = start.format('DD-MM-YYYY HH:mm');
                                    document.getElementById("endEvent").value = end.format('DD-MM-YYYY HH:mm');
                                    return;
                                });
                                document.getElementById("startEvent").value = $('#dateRangePicker').data('daterangepicker').startDate.format('DD-MM-YYYY HH:mm');
                                document.getElementById("endEvent").value = $('#dateRangePicker').data('daterangepicker').endDate.format('DD-MM-YYYY HH:mm');
                            });

                        </script>

                        <%--LIEU--%>
                        <div class="form-group">
                            <div id="Automatic" class="Automatic" runat="server" visible="true">
                                <label for="locationField">Adresse :</label>
                                <div id="locationField">
                                    <input id="autocomplete" style="margin-bottom: 20px;" runat="server" class="form-control" clientidmode="Static"
                                        placeholder="Entrez l'addresse de l'evenement" type="text" />
                                </div>
                            </div>
                            <asp:Button ID="btnCustom" runat="server" CssClass="btn btn-default" OnClick="btnCustom_Click" Text="Saisir l'adresse manuellement" Visible="true"></asp:Button>
                            <%--<a id="btnCustom" style="margin-top: 20px;" onclick="ShowCustom()" class="btn btn-default" data-toggle="collapse"></a>--%>
                            <div id="CustomAdresse" runat="server" clientidmode="Static" class="form-group customAdresse" visible="false">
                                <div style="margin-top: 20px;">
                                    <div class="form-group col-xs-12 col-sm-5 col-md-4 col-lg-4">
                                        <label>Numéro de rue</label>
                                        <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputNumberStreet" />
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-7 col-md-8 col-lg-8">
                                        <label>Nom de rue</label>
                                        <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputNameStreet" />
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label>Ville</label>
                                        <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputlocality" />
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label>Code postal</label>
                                        <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputPostalCode" />
                                    </div>
                                    <div class="form-group col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <label>Pays</label>
                                        <input runat="server" type="text" clientidmode="Static" class="form-control" id="inputCountry" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:HiddenField ID="street_number" Value="" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="route" Value="" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="locality" Value="" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="administrative_area_level_1" ClientIDMode="Static" Value="" runat="server" />
                        <asp:HiddenField ID="country" Value="" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="postal_code" Value="" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="startEvent" Value="" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="endEvent" Value="" ClientIDMode="Static" runat="server" />


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"></div>
                            <asp:LinkButton runat="server" OnClick="btnCreateEvent_Click" CssClass="col-lg-4 col-md-4 col-sm-4 col-xs-12 btn btn-success" ID="btnCreateEvent" Text="Créer" />
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>



    <script>

        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                { types: ['geocode'] });
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
        function ShowMessage(message, messagetype) {
            var cssclass;
            switch (messagetype) {
                case 'success':
                    cssclass = 'alert-success';
                    break;
                case 'error':
                    cssclass = 'alert-danger';
                    break;
                case 'warning':
                    cssclass = 'alert-warning';
                    break;
                default:
                    cssclass = 'alert-info';
            }

            $('#Body').before('<div id="alert_div" style="position: fixed; left: 50%;width: 50%;margin-top: 20px;transform: translateX(-50%);" class="alert fade in ' + cssclass + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><span>' + message + '</span></div>');
            $(".alert").fadeTo(5000, 0).slideUp(1500, function () {
                $(this).remove();
            });
        }
        function ShowCustom() {
            try {
                if ($(".customAdresse").is(":hidden")) {
                    $(".Automatic").slideToggle("slow");

                    $("#btnCustom").text("Rechercher l'adresse avec google");
                }
                else {
                    $(".Automatic").slideToggle("slow");
                    $("#btnCustom").text("Saisir l'adresse manuellement");
                }
            }
            catch (e) {

            }

        }

        function LoadDateRange()
        {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $('#dateRangePicker').daterangepicker({
                timePicker: true,
                "minYear": 2018,
                "maxYear": 2100,
                "locale": {
                    "format": "DD/MM/YYYY HH:mm",
                    "separator": " --> ",
                    "applyLabel": "Valider",
                    "cancelLabel": "Annuler",
                    "fromLabel": "De",
                    "toLabel": "à",
                    "customRangeLabel": "Custom",
                    "weekLabel": "W",
                    "daysOfWeek": [
                        "Lun",
                        "Mar",
                        "Mer",
                        "Jeu",
                        "Ven",
                        "Sam",
                        "Dim"
                    ],
                    "monthNames": [
                        "Janvier",
                        "Février",
                        "Mars",
                        "Avril",
                        "Mai",
                        "Juin",
                        "Juillet",
                        "Août",
                        "Septembre",
                        "Octobre",
                        "Novembre",
                        "Decembre"
                    ],
                    "firstDay": 1
                },
                "parentEl": "formDate",
                "startDate": dd + "-" + mm + "-" + yyyy + " " + 18 + ":" + 00,
                "endDate": dd + 3 + "-" + mm + "-" + yyyy + " " + 20 + ":" + 00,
                "cancelClass": "btn-danger",
                timePicker24Hour: true
            }, function (start, end, label) {
                document.getElementById("startEvent").value = start.format('DD-MM-YYYY HH:mm');
                document.getElementById("endEvent").value = end.format('DD-MM-YYYY HH:mm');
                return;
            });
            document.getElementById("startEvent").value = $('#dateRangePicker').data('daterangepicker').startDate.format('DD-MM-YYYY HH:mm');
            document.getElementById("endEvent").value = $('#dateRangePicker').data('daterangepicker').endDate.format('DD-MM-YYYY HH:mm');
        }
        
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZlUHhRldlOcuivtz25VGMQ733ZfT0YL4&libraries=places&callback=initAutocomplete" async defer></script>

</asp:Content>
