﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_vent
{
    public partial class CreateEvent : PageBase
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            isAuthenticated();

        }

        protected void btnCreateEvent_Click(object sender, EventArgs e)
        {
            try
            {


                string message = "";
                string numRueInput = "";
                string villeInput = "";
                string paysInput = "";
                string rueInput = "";
                string cpInput = "";
                if (Automatic.Visible)
                {
                    numRueInput = street_number.Value.Trim();
                    villeInput = locality.Value.Trim();
                    paysInput = country.Value.Trim();
                    rueInput = route.Value.Trim();
                    cpInput = postal_code.Value.Trim();
                }
                else
                {
                    numRueInput = inputNumberStreet.Value;
                    villeInput = inputlocality.Value;
                    paysInput = inputCountry.Value;
                    rueInput = inputNameStreet.Value;
                    cpInput = inputPostalCode.Value;
                }

                string nomEventInput = nomEvent.Value.Trim();
                CultureInfo culture = new CultureInfo("fr-FR");
                string format = "dd-MM-yyyy HH:mm";
                string debut = startEvent.Value;
                string fin = endEvent.Value;



                DateTime debutEvent = DateTime.ParseExact(debut, format, CultureInfo.InvariantCulture);
                DateTime finEvent = DateTime.ParseExact(fin, format, CultureInfo.InvariantCulture);

                if (!string.IsNullOrEmpty(villeInput) && !string.IsNullOrEmpty(paysInput) && !string.IsNullOrEmpty(rueInput) && !string.IsNullOrEmpty(cpInput) && !string.IsNullOrEmpty(nomEventInput))
                {
                    Evenement event1;
                    if (string.IsNullOrEmpty(numRueInput) && !string.IsNullOrEmpty(cpInput))
                    {
                        int cp;
                        if (!int.TryParse(cpInput, out cp))
                        {
                            cp = 0;
                        }
                        event1 = new Evenement(nomEventInput, cp, villeInput, paysInput, rueInput, debutEvent, finEvent);
                    }
                    else if (string.IsNullOrEmpty(cpInput) && !string.IsNullOrEmpty(numRueInput))
                    {
                        event1 = new Evenement(nomEventInput, villeInput, paysInput, rueInput, int.Parse(numRueInput), debutEvent, finEvent);
                    }
                    else
                    {
                        int cp;
                        if (!int.TryParse(cpInput, out cp))
                        {
                            cp = 0;
                        }
                        event1 = new Evenement(nomEventInput, cp, villeInput, paysInput, rueInput, int.Parse(numRueInput), debutEvent, finEvent);
                    }


                    if (event1.dateDebut != null && event1.dateFin != null && !string.IsNullOrEmpty(event1.nom)
                         && !string.IsNullOrEmpty(event1.ville)
                        && !string.IsNullOrEmpty(event1.rue) && !string.IsNullOrEmpty(event1.pays))
                    {
                        DBConnect db = new DBConnect();
                        db.Initialize();
                        if (db.AddEvent(event1, out message))
                        {
                            ShowMessage("Evenement crée avec succes !", "success");
                        }
                        else
                        {
                            ShowMessage(message, "error");
                        }
                    }
                }
                else
                {
                    ShowMessage("Un des champs est manquant ou incorect.", "error");
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex.Message);
            }
        }

        protected void btnCustom_Click(object sender, EventArgs e)
        {
            if(CustomAdresse.Visible)
            {
                CustomAdresse.Visible = false;
                Automatic.Visible = true;
                btnCustom.Text = "Rechercher l'adresse avec google";
            }
            else
            {
                CustomAdresse.Visible = true;
                Automatic.Visible = false;
                btnCustom.Text = "Saisir l'adresse manuellement";
            }

        }
    }
}